<?php

// Data functions (insert, update, delete, form) for table Club_VolunteerDocumentation

// This script and data application were generated by AppGini 5.82
// Download AppGini for free from https://bigprof.com/appgini/download/

function Club_VolunteerDocumentation_insert() {
	global $Translation;

	// mm: can member insert record?
	$arrPerm = getTablePermissions('Club_VolunteerDocumentation');
	if(!$arrPerm[1]) return false;

	$data = array();
	$data['VolunteerId'] = $_REQUEST['VolunteerId'];
		if($data['VolunteerId'] == empty_lookup_value) { $data['VolunteerId'] = ''; }
	$data['DocumentDate'] = intval($_REQUEST['DocumentDateYear']) . '-' . intval($_REQUEST['DocumentDateMonth']) . '-' . intval($_REQUEST['DocumentDateDay']);
	$data['DocumentDate'] = parseMySQLDate($data['DocumentDate'], '');
	$data['DocumentExpiryDate'] = intval($_REQUEST['DocumentExpiryDateYear']) . '-' . intval($_REQUEST['DocumentExpiryDateMonth']) . '-' . intval($_REQUEST['DocumentExpiryDateDay']);
	$data['DocumentExpiryDate'] = parseMySQLDate($data['DocumentExpiryDate'], '');
	$data['DocumentType'] = $_REQUEST['DocumentType'];
		if($data['DocumentType'] == empty_lookup_value) { $data['DocumentType'] = ''; }
	$data['Notes'] = $_REQUEST['Notes'];
		if($data['Notes'] == empty_lookup_value) { $data['Notes'] = ''; }
	$data['DocumentLink'] = PrepareUploadedFile('DocumentLink', 5120000,'txt|doc|docx|docm|odt|pdf|rtf', false, '');

	/* for empty upload fields, when saving a copy of an existing record, copy the original upload field */
	if($_REQUEST['SelectedID']) {
		$res = sql("select * from Club_VolunteerDocumentation where Id='" . makeSafe($_REQUEST['SelectedID']) . "'", $eo);
		if($row = db_fetch_assoc($res)) {
			if(!$data['DocumentLink']) $data['DocumentLink'] = $row['DocumentLink'];
		}
	}

	// hook: Club_VolunteerDocumentation_before_insert
	if(function_exists('Club_VolunteerDocumentation_before_insert')) {
		$args = array();
		if(!Club_VolunteerDocumentation_before_insert($data, getMemberInfo(), $args)) { return false; }
	}

	$error = '';
	// set empty fields to NULL
	$data = array_map(function($v) { return ($v === '' ? NULL : $v); }, $data);
	insert('Club_VolunteerDocumentation', backtick_keys_once($data), $error);
	if($error)
		die("{$error}<br><a href=\"#\" onclick=\"history.go(-1);\">{$Translation['< back']}</a>");

	$recID = db_insert_id(db_link());

	// hook: Club_VolunteerDocumentation_after_insert
	if(function_exists('Club_VolunteerDocumentation_after_insert')) {
		$res = sql("select * from `Club_VolunteerDocumentation` where `Id`='" . makeSafe($recID, false) . "' limit 1", $eo);
		if($row = db_fetch_assoc($res)) {
			$data = array_map('makeSafe', $row);
		}
		$data['selectedID'] = makeSafe($recID, false);
		$args=array();
		if(!Club_VolunteerDocumentation_after_insert($data, getMemberInfo(), $args)) { return $recID; }
	}

	// mm: save ownership data
	set_record_owner('Club_VolunteerDocumentation', $recID, getLoggedMemberID());

	// if this record is a copy of another record, copy children if applicable
	if(!empty($_REQUEST['SelectedID'])) Club_VolunteerDocumentation_copy_children($recID, $_REQUEST['SelectedID']);

	return $recID;
}

function Club_VolunteerDocumentation_copy_children($destination_id, $source_id) {
	global $Translation;
	$requests = array(); // array of curl handlers for launching insert requests
	$eo = array('silentErrors' => true);
	$uploads_dir = realpath(dirname(__FILE__) . '/../' . $Translation['ImageFolder']);
	$safe_sid = makeSafe($source_id);

	// launch requests, asynchronously
	curl_batch($requests);
}

function Club_VolunteerDocumentation_delete($selected_id, $AllowDeleteOfParents=false, $skipChecks=false) {
	// insure referential integrity ...
	global $Translation;
	$selected_id=makeSafe($selected_id);

	// mm: can member delete record?
	$arrPerm=getTablePermissions('Club_VolunteerDocumentation');
	$ownerGroupID=sqlValue("select groupID from membership_userrecords where tableName='Club_VolunteerDocumentation' and pkValue='$selected_id'");
	$ownerMemberID=sqlValue("select lcase(memberID) from membership_userrecords where tableName='Club_VolunteerDocumentation' and pkValue='$selected_id'");
	if(($arrPerm[4]==1 && $ownerMemberID==getLoggedMemberID()) || ($arrPerm[4]==2 && $ownerGroupID==getLoggedGroupID()) || $arrPerm[4]==3) { // allow delete?
		// delete allowed, so continue ...
	}else{
		return $Translation['You don\'t have enough permissions to delete this record'];
	}

	// hook: Club_VolunteerDocumentation_before_delete
	if(function_exists('Club_VolunteerDocumentation_before_delete')) {
		$args=array();
		if(!Club_VolunteerDocumentation_before_delete($selected_id, $skipChecks, getMemberInfo(), $args))
			return $Translation['Couldn\'t delete this record'];
	}

	// delete file stored in the 'DocumentLink' field
	$res = sql("select `DocumentLink` from `Club_VolunteerDocumentation` where `Id`='$selected_id'", $eo);
	if($row=@db_fetch_row($res)) {
		if($row[0]!='') {
			@unlink(getUploadDir('').$row[0]);
		}
	}

	sql("delete from `Club_VolunteerDocumentation` where `Id`='$selected_id'", $eo);

	// hook: Club_VolunteerDocumentation_after_delete
	if(function_exists('Club_VolunteerDocumentation_after_delete')) {
		$args=array();
		Club_VolunteerDocumentation_after_delete($selected_id, getMemberInfo(), $args);
	}

	// mm: delete ownership data
	sql("delete from membership_userrecords where tableName='Club_VolunteerDocumentation' and pkValue='$selected_id'", $eo);
}

function Club_VolunteerDocumentation_update($selected_id) {
	global $Translation;

	// mm: can member edit record?
	$arrPerm=getTablePermissions('Club_VolunteerDocumentation');
	$ownerGroupID=sqlValue("select groupID from membership_userrecords where tableName='Club_VolunteerDocumentation' and pkValue='".makeSafe($selected_id)."'");
	$ownerMemberID=sqlValue("select lcase(memberID) from membership_userrecords where tableName='Club_VolunteerDocumentation' and pkValue='".makeSafe($selected_id)."'");
	if(($arrPerm[3]==1 && $ownerMemberID==getLoggedMemberID()) || ($arrPerm[3]==2 && $ownerGroupID==getLoggedGroupID()) || $arrPerm[3]==3) { // allow update?
		// update allowed, so continue ...
	}else{
		return false;
	}

	$data['VolunteerId'] = makeSafe($_REQUEST['VolunteerId']);
		if($data['VolunteerId'] == empty_lookup_value) { $data['VolunteerId'] = ''; }
	$data['DocumentDate'] = intval($_REQUEST['DocumentDateYear']) . '-' . intval($_REQUEST['DocumentDateMonth']) . '-' . intval($_REQUEST['DocumentDateDay']);
	$data['DocumentDate'] = parseMySQLDate($data['DocumentDate'], '');
	$data['DocumentExpiryDate'] = intval($_REQUEST['DocumentExpiryDateYear']) . '-' . intval($_REQUEST['DocumentExpiryDateMonth']) . '-' . intval($_REQUEST['DocumentExpiryDateDay']);
	$data['DocumentExpiryDate'] = parseMySQLDate($data['DocumentExpiryDate'], '');
	$data['DocumentType'] = makeSafe($_REQUEST['DocumentType']);
		if($data['DocumentType'] == empty_lookup_value) { $data['DocumentType'] = ''; }
	$data['Notes'] = makeSafe($_REQUEST['Notes']);
		if($data['Notes'] == empty_lookup_value) { $data['Notes'] = ''; }
	$data['selectedID'] = makeSafe($selected_id);
	if($_REQUEST['DocumentLink_remove'] == 1) {
		$data['DocumentLink'] = '';
		// delete file from server
		$res = sql("select `DocumentLink` from `Club_VolunteerDocumentation` where `Id`='".makeSafe($selected_id)."'", $eo);
		if($row = @db_fetch_row($res)) {
			if($row[0] != '') {
				@unlink(getUploadDir('') . $row[0]);
			}
		}
	} else {
		$data['DocumentLink'] = PrepareUploadedFile('DocumentLink', 5120000, 'txt|doc|docx|docm|odt|pdf|rtf', false, "");
		// delete file from server
		if($data['DocumentLink'] != '') {
			$res = sql("select `DocumentLink` from `Club_VolunteerDocumentation` where `Id`='" . makeSafe($selected_id) . "'", $eo);
			if($row = @db_fetch_row($res)) {
				if($row[0] != '') {
					@unlink(getUploadDir('') . $row[0]);
				}
			}
		}
	}

	// hook: Club_VolunteerDocumentation_before_update
	if(function_exists('Club_VolunteerDocumentation_before_update')) {
		$args = array();
		if(!Club_VolunteerDocumentation_before_update($data, getMemberInfo(), $args)) { return false; }
	}

	$o = array('silentErrors' => true);
	sql('update `Club_VolunteerDocumentation` set       `VolunteerId`=' . (($data['VolunteerId'] !== '' && $data['VolunteerId'] !== NULL) ? "'{$data['VolunteerId']}'" : 'NULL') . ', `DocumentDate`=' . (($data['DocumentDate'] !== '' && $data['DocumentDate'] !== NULL) ? "'{$data['DocumentDate']}'" : 'NULL') . ', `DocumentExpiryDate`=' . (($data['DocumentExpiryDate'] !== '' && $data['DocumentExpiryDate'] !== NULL) ? "'{$data['DocumentExpiryDate']}'" : 'NULL') . ', `DocumentType`=' . (($data['DocumentType'] !== '' && $data['DocumentType'] !== NULL) ? "'{$data['DocumentType']}'" : 'NULL') . ', ' . ($data['DocumentLink']!='' ? "`DocumentLink`='{$data['DocumentLink']}'" : ($_REQUEST['DocumentLink_remove'] != 1 ? '`DocumentLink`=`DocumentLink`' : '`DocumentLink`=NULL')) . ', `Notes`=' . (($data['Notes'] !== '' && $data['Notes'] !== NULL) ? "'{$data['Notes']}'" : 'NULL') . " where `Id`='".makeSafe($selected_id)."'", $o);
	if($o['error']!='') {
		echo $o['error'];
		echo '<a href="Club_VolunteerDocumentation_view.php?SelectedID='.urlencode($selected_id)."\">{$Translation['< back']}</a>";
		exit;
	}


	// hook: Club_VolunteerDocumentation_after_update
	if(function_exists('Club_VolunteerDocumentation_after_update')) {
		$res = sql("SELECT * FROM `Club_VolunteerDocumentation` WHERE `Id`='{$data['selectedID']}' LIMIT 1", $eo);
		if($row = db_fetch_assoc($res)) {
			$data = array_map('makeSafe', $row);
		}
		$data['selectedID'] = $data['Id'];
		$args = array();
		if(!Club_VolunteerDocumentation_after_update($data, getMemberInfo(), $args)) { return; }
	}

	// mm: update ownership data
	sql("update `membership_userrecords` set `dateUpdated`='" . time() . "' where `tableName`='Club_VolunteerDocumentation' and `pkValue`='" . makeSafe($selected_id) . "'", $eo);

}

function Club_VolunteerDocumentation_form($selected_id = '', $AllowUpdate = 1, $AllowInsert = 1, $AllowDelete = 1, $ShowCancel = 0, $TemplateDV = '', $TemplateDVP = '') {
	// function to return an editable form for a table records
	// and fill it with data of record whose ID is $selected_id. If $selected_id
	// is empty, an empty form is shown, with only an 'Add New'
	// button displayed.

	global $Translation;

	// mm: get table permissions
	$arrPerm=getTablePermissions('Club_VolunteerDocumentation');
	if(!$arrPerm[1] && $selected_id=='') { return ''; }
	$AllowInsert = ($arrPerm[1] ? true : false);
	// print preview?
	$dvprint = false;
	if($selected_id && $_REQUEST['dvprint_x'] != '') {
		$dvprint = true;
	}

	$filterer_VolunteerId = thisOr(undo_magic_quotes($_REQUEST['filterer_VolunteerId']), '');

	// populate filterers, starting from children to grand-parents

	// unique random identifier
	$rnd1 = ($dvprint ? rand(1000000, 9999999) : '');
	// combobox: VolunteerId
	$combo_VolunteerId = new DataCombo;
	// combobox: DocumentDate
	$combo_DocumentDate = new DateCombo;
	$combo_DocumentDate->DateFormat = "dmy";
	$combo_DocumentDate->MinYear = 1900;
	$combo_DocumentDate->MaxYear = 2100;
	$combo_DocumentDate->DefaultDate = parseMySQLDate('', '');
	$combo_DocumentDate->MonthNames = $Translation['month names'];
	$combo_DocumentDate->NamePrefix = 'DocumentDate';
	// combobox: DocumentExpiryDate
	$combo_DocumentExpiryDate = new DateCombo;
	$combo_DocumentExpiryDate->DateFormat = "dmy";
	$combo_DocumentExpiryDate->MinYear = 1900;
	$combo_DocumentExpiryDate->MaxYear = 2100;
	$combo_DocumentExpiryDate->DefaultDate = parseMySQLDate('', '');
	$combo_DocumentExpiryDate->MonthNames = $Translation['month names'];
	$combo_DocumentExpiryDate->NamePrefix = 'DocumentExpiryDate';
	// combobox: DocumentType
	$combo_DocumentType = new Combo;
	$combo_DocumentType->ListType = 0;
	$combo_DocumentType->MultipleSeparator = ', ';
	$combo_DocumentType->ListBoxHeight = 10;
	$combo_DocumentType->RadiosPerLine = 1;
	if(is_file(dirname(__FILE__).'/hooks/Club_VolunteerDocumentation.DocumentType.csv')) {
		$DocumentType_data = addslashes(implode('', @file(dirname(__FILE__).'/hooks/Club_VolunteerDocumentation.DocumentType.csv')));
		$combo_DocumentType->ListItem = explode('||', entitiesToUTF8(convertLegacyOptions($DocumentType_data)));
		$combo_DocumentType->ListData = $combo_DocumentType->ListItem;
	}else{
		$combo_DocumentType->ListItem = explode('||', entitiesToUTF8(convertLegacyOptions("DBS;;First Aid;;Coaching Certificate;;Safeguarding Certificate;;Other")));
		$combo_DocumentType->ListData = $combo_DocumentType->ListItem;
	}
	$combo_DocumentType->SelectName = 'DocumentType';

	if($selected_id) {
		// mm: check member permissions
		if(!$arrPerm[2]) {
			return "";
		}
		// mm: who is the owner?
		$ownerGroupID=sqlValue("select groupID from membership_userrecords where tableName='Club_VolunteerDocumentation' and pkValue='".makeSafe($selected_id)."'");
		$ownerMemberID=sqlValue("select lcase(memberID) from membership_userrecords where tableName='Club_VolunteerDocumentation' and pkValue='".makeSafe($selected_id)."'");
		if($arrPerm[2]==1 && getLoggedMemberID()!=$ownerMemberID) {
			return "";
		}
		if($arrPerm[2]==2 && getLoggedGroupID()!=$ownerGroupID) {
			return "";
		}

		// can edit?
		if(($arrPerm[3]==1 && $ownerMemberID==getLoggedMemberID()) || ($arrPerm[3]==2 && $ownerGroupID==getLoggedGroupID()) || $arrPerm[3]==3) {
			$AllowUpdate=1;
		}else{
			$AllowUpdate=0;
		}

		$res = sql("SELECT * FROM `Club_VolunteerDocumentation` WHERE `Id`='" . makeSafe($selected_id) . "'", $eo);
		if(!($row = db_fetch_array($res))) {
			return error_message($Translation['No records found'], 'Club_VolunteerDocumentation_view.php', false);
		}
		$combo_VolunteerId->SelectedData = $row['VolunteerId'];
		$combo_DocumentDate->DefaultDate = $row['DocumentDate'];
		$combo_DocumentExpiryDate->DefaultDate = $row['DocumentExpiryDate'];
		$combo_DocumentType->SelectedData = $row['DocumentType'];
		$urow = $row; /* unsanitized data */
		$hc = new CI_Input();
		$row = $hc->xss_clean($row); /* sanitize data */
	} else {
		$combo_VolunteerId->SelectedData = $filterer_VolunteerId;
		$combo_DocumentType->SelectedText = ( $_REQUEST['FilterField'][1]=='5' && $_REQUEST['FilterOperator'][1]=='<=>' ? (get_magic_quotes_gpc() ? stripslashes($_REQUEST['FilterValue'][1]) : $_REQUEST['FilterValue'][1]) : "");
	}
	$combo_VolunteerId->HTML = '<span id="VolunteerId-container' . $rnd1 . '"></span><input type="hidden" name="VolunteerId" id="VolunteerId' . $rnd1 . '" value="' . html_attr($combo_VolunteerId->SelectedData) . '">';
	$combo_VolunteerId->MatchText = '<span id="VolunteerId-container-readonly' . $rnd1 . '"></span><input type="hidden" name="VolunteerId" id="VolunteerId' . $rnd1 . '" value="' . html_attr($combo_VolunteerId->SelectedData) . '">';
	$combo_DocumentType->Render();

	ob_start();
	?>

	<script>
		// initial lookup values
		AppGini.current_VolunteerId__RAND__ = { text: "", value: "<?php echo addslashes($selected_id ? $urow['VolunteerId'] : $filterer_VolunteerId); ?>"};

		jQuery(function() {
			setTimeout(function() {
				if(typeof(VolunteerId_reload__RAND__) == 'function') VolunteerId_reload__RAND__();
			}, 10); /* we need to slightly delay client-side execution of the above code to allow AppGini.ajaxCache to work */
		});
		function VolunteerId_reload__RAND__() {
		<?php if(($AllowUpdate || $AllowInsert) && !$dvprint) { ?>

			$j("#VolunteerId-container__RAND__").select2({
				/* initial default value */
				initSelection: function(e, c) {
					$j.ajax({
						url: 'ajax_combo.php',
						dataType: 'json',
						data: { id: AppGini.current_VolunteerId__RAND__.value, t: 'Club_VolunteerDocumentation', f: 'VolunteerId' },
						success: function(resp) {
							c({
								id: resp.results[0].id,
								text: resp.results[0].text
							});
							$j('[name="VolunteerId"]').val(resp.results[0].id);
							$j('[id=VolunteerId-container-readonly__RAND__]').html('<span id="VolunteerId-match-text">' + resp.results[0].text + '</span>');
							if(resp.results[0].id == '<?php echo empty_lookup_value; ?>') { $j('.btn[id=Club_Volunteers_view_parent]').hide(); }else{ $j('.btn[id=Club_Volunteers_view_parent]').show(); }


							if(typeof(VolunteerId_update_autofills__RAND__) == 'function') VolunteerId_update_autofills__RAND__();
						}
					});
				},
				width: '100%',
				formatNoMatches: function(term) { /* */ return '<?php echo addslashes($Translation['No matches found!']); ?>'; },
				minimumResultsForSearch: 5,
				loadMorePadding: 200,
				ajax: {
					url: 'ajax_combo.php',
					dataType: 'json',
					cache: true,
					data: function(term, page) { /* */ return { s: term, p: page, t: 'Club_VolunteerDocumentation', f: 'VolunteerId' }; },
					results: function(resp, page) { /* */ return resp; }
				},
				escapeMarkup: function(str) { /* */ return str; }
			}).on('change', function(e) {
				AppGini.current_VolunteerId__RAND__.value = e.added.id;
				AppGini.current_VolunteerId__RAND__.text = e.added.text;
				$j('[name="VolunteerId"]').val(e.added.id);
				if(e.added.id == '<?php echo empty_lookup_value; ?>') { $j('.btn[id=Club_Volunteers_view_parent]').hide(); }else{ $j('.btn[id=Club_Volunteers_view_parent]').show(); }


				if(typeof(VolunteerId_update_autofills__RAND__) == 'function') VolunteerId_update_autofills__RAND__();
			});

			if(!$j("#VolunteerId-container__RAND__").length) {
				$j.ajax({
					url: 'ajax_combo.php',
					dataType: 'json',
					data: { id: AppGini.current_VolunteerId__RAND__.value, t: 'Club_VolunteerDocumentation', f: 'VolunteerId' },
					success: function(resp) {
						$j('[name="VolunteerId"]').val(resp.results[0].id);
						$j('[id=VolunteerId-container-readonly__RAND__]').html('<span id="VolunteerId-match-text">' + resp.results[0].text + '</span>');
						if(resp.results[0].id == '<?php echo empty_lookup_value; ?>') { $j('.btn[id=Club_Volunteers_view_parent]').hide(); }else{ $j('.btn[id=Club_Volunteers_view_parent]').show(); }

						if(typeof(VolunteerId_update_autofills__RAND__) == 'function') VolunteerId_update_autofills__RAND__();
					}
				});
			}

		<?php }else{ ?>

			$j.ajax({
				url: 'ajax_combo.php',
				dataType: 'json',
				data: { id: AppGini.current_VolunteerId__RAND__.value, t: 'Club_VolunteerDocumentation', f: 'VolunteerId' },
				success: function(resp) {
					$j('[id=VolunteerId-container__RAND__], [id=VolunteerId-container-readonly__RAND__]').html('<span id="VolunteerId-match-text">' + resp.results[0].text + '</span>');
					if(resp.results[0].id == '<?php echo empty_lookup_value; ?>') { $j('.btn[id=Club_Volunteers_view_parent]').hide(); }else{ $j('.btn[id=Club_Volunteers_view_parent]').show(); }

					if(typeof(VolunteerId_update_autofills__RAND__) == 'function') VolunteerId_update_autofills__RAND__();
				}
			});
		<?php } ?>

		}
	</script>
	<?php

	$lookups = str_replace('__RAND__', $rnd1, ob_get_contents());
	ob_end_clean();


	// code for template based detail view forms

	// open the detail view template
	if($dvprint) {
		$template_file = is_file("./{$TemplateDVP}") ? "./{$TemplateDVP}" : './templates/Club_VolunteerDocumentation_templateDVP.html';
		$templateCode = @file_get_contents($template_file);
	}else{
		$template_file = is_file("./{$TemplateDV}") ? "./{$TemplateDV}" : './templates/Club_VolunteerDocumentation_templateDV.html';
		$templateCode = @file_get_contents($template_file);
	}

	// process form title
	$templateCode = str_replace('<%%DETAIL_VIEW_TITLE%%>', 'Volunteer Documentation Details', $templateCode);
	$templateCode = str_replace('<%%RND1%%>', $rnd1, $templateCode);
	$templateCode = str_replace('<%%EMBEDDED%%>', ($_REQUEST['Embedded'] ? 'Embedded=1' : ''), $templateCode);
	// process buttons
	if($AllowInsert) {
		if(!$selected_id) $templateCode = str_replace('<%%INSERT_BUTTON%%>', '<button type="submit" class="btn btn-success" id="insert" name="insert_x" value="1" onclick="return Club_VolunteerDocumentation_validateData();"><i class="glyphicon glyphicon-plus-sign"></i> ' . $Translation['Save New'] . '</button>', $templateCode);
		$templateCode = str_replace('<%%INSERT_BUTTON%%>', '<button type="submit" class="btn btn-default" id="insert" name="insert_x" value="1" onclick="return Club_VolunteerDocumentation_validateData();"><i class="glyphicon glyphicon-plus-sign"></i> ' . $Translation['Save As Copy'] . '</button>', $templateCode);
	}else{
		$templateCode = str_replace('<%%INSERT_BUTTON%%>', '', $templateCode);
	}

	// 'Back' button action
	if($_REQUEST['Embedded']) {
		$backAction = 'AppGini.closeParentModal(); return false;';
	}else{
		$backAction = '$j(\'form\').eq(0).attr(\'novalidate\', \'novalidate\'); document.myform.reset(); return true;';
	}

	if($selected_id) {
		if(!$_REQUEST['Embedded']) $templateCode = str_replace('<%%DVPRINT_BUTTON%%>', '<button type="submit" class="btn btn-default" id="dvprint" name="dvprint_x" value="1" onclick="$j(\'form\').eq(0).prop(\'novalidate\', true); document.myform.reset(); return true;" title="' . html_attr($Translation['Print Preview']) . '"><i class="glyphicon glyphicon-print"></i> ' . $Translation['Print Preview'] . '</button>', $templateCode);
		if($AllowUpdate) {
			$templateCode = str_replace('<%%UPDATE_BUTTON%%>', '<button type="submit" class="btn btn-success btn-lg" id="update" name="update_x" value="1" onclick="return Club_VolunteerDocumentation_validateData();" title="' . html_attr($Translation['Save Changes']) . '"><i class="glyphicon glyphicon-ok"></i> ' . $Translation['Save Changes'] . '</button>', $templateCode);
		}else{
			$templateCode = str_replace('<%%UPDATE_BUTTON%%>', '', $templateCode);
		}
		if(($arrPerm[4]==1 && $ownerMemberID==getLoggedMemberID()) || ($arrPerm[4]==2 && $ownerGroupID==getLoggedGroupID()) || $arrPerm[4]==3) { // allow delete?
			$templateCode = str_replace('<%%DELETE_BUTTON%%>', '<button type="submit" class="btn btn-danger" id="delete" name="delete_x" value="1" onclick="return confirm(\'' . $Translation['are you sure?'] . '\');" title="' . html_attr($Translation['Delete']) . '"><i class="glyphicon glyphicon-trash"></i> ' . $Translation['Delete'] . '</button>', $templateCode);
		}else{
			$templateCode = str_replace('<%%DELETE_BUTTON%%>', '', $templateCode);
		}
		$templateCode = str_replace('<%%DESELECT_BUTTON%%>', '<button type="submit" class="btn btn-default" id="deselect" name="deselect_x" value="1" onclick="' . $backAction . '" title="' . html_attr($Translation['Back']) . '"><i class="glyphicon glyphicon-chevron-left"></i> ' . $Translation['Back'] . '</button>', $templateCode);
	}else{
		$templateCode = str_replace('<%%UPDATE_BUTTON%%>', '', $templateCode);
		$templateCode = str_replace('<%%DELETE_BUTTON%%>', '', $templateCode);
		$templateCode = str_replace('<%%DESELECT_BUTTON%%>', ($ShowCancel ? '<button type="submit" class="btn btn-default" id="deselect" name="deselect_x" value="1" onclick="' . $backAction . '" title="' . html_attr($Translation['Back']) . '"><i class="glyphicon glyphicon-chevron-left"></i> ' . $Translation['Back'] . '</button>' : ''), $templateCode);
	}

	// set records to read only if user can't insert new records and can't edit current record
	if(($selected_id && !$AllowUpdate && !$AllowInsert) || (!$selected_id && !$AllowInsert)) {
		$jsReadOnly .= "\tjQuery('#VolunteerId').prop('disabled', true).css({ color: '#555', backgroundColor: '#fff' });\n";
		$jsReadOnly .= "\tjQuery('#VolunteerId_caption').prop('disabled', true).css({ color: '#555', backgroundColor: 'white' });\n";
		$jsReadOnly .= "\tjQuery('#DocumentDate').prop('readonly', true);\n";
		$jsReadOnly .= "\tjQuery('#DocumentDateDay, #DocumentDateMonth, #DocumentDateYear').prop('disabled', true).css({ color: '#555', backgroundColor: '#fff' });\n";
		$jsReadOnly .= "\tjQuery('#DocumentExpiryDate').prop('readonly', true);\n";
		$jsReadOnly .= "\tjQuery('#DocumentExpiryDateDay, #DocumentExpiryDateMonth, #DocumentExpiryDateYear').prop('disabled', true).css({ color: '#555', backgroundColor: '#fff' });\n";
		$jsReadOnly .= "\tjQuery('#DocumentType').replaceWith('<div class=\"form-control-static\" id=\"DocumentType\">' + (jQuery('#DocumentType').val() || '') + '</div>'); jQuery('#DocumentType-multi-selection-help').hide();\n";
		$jsReadOnly .= "\tjQuery('#DocumentLink').replaceWith('<div class=\"form-control-static\" id=\"DocumentLink\">' + (jQuery('#DocumentLink').val() || '') + '</div>');\n";
		$jsReadOnly .= "\tjQuery('#DocumentLink, #DocumentLink-edit-link').hide();\n";
		$jsReadOnly .= "\tjQuery('.select2-container').hide();\n";

		$noUploads = true;
	}elseif($AllowInsert) {
		$jsEditable .= "\tjQuery('form').eq(0).data('already_changed', true);"; // temporarily disable form change handler
			$jsEditable .= "\tjQuery('form').eq(0).data('already_changed', false);"; // re-enable form change handler
	}

	// process combos
	$templateCode = str_replace('<%%COMBO(VolunteerId)%%>', $combo_VolunteerId->HTML, $templateCode);
	$templateCode = str_replace('<%%COMBOTEXT(VolunteerId)%%>', $combo_VolunteerId->MatchText, $templateCode);
	$templateCode = str_replace('<%%URLCOMBOTEXT(VolunteerId)%%>', urlencode($combo_VolunteerId->MatchText), $templateCode);
	$templateCode = str_replace('<%%COMBO(DocumentDate)%%>', ($selected_id && !$arrPerm[3] ? '<div class="form-control-static">' . $combo_DocumentDate->GetHTML(true) . '</div>' : $combo_DocumentDate->GetHTML()), $templateCode);
	$templateCode = str_replace('<%%COMBOTEXT(DocumentDate)%%>', $combo_DocumentDate->GetHTML(true), $templateCode);
	$templateCode = str_replace('<%%COMBO(DocumentExpiryDate)%%>', ($selected_id && !$arrPerm[3] ? '<div class="form-control-static">' . $combo_DocumentExpiryDate->GetHTML(true) . '</div>' : $combo_DocumentExpiryDate->GetHTML()), $templateCode);
	$templateCode = str_replace('<%%COMBOTEXT(DocumentExpiryDate)%%>', $combo_DocumentExpiryDate->GetHTML(true), $templateCode);
	$templateCode = str_replace('<%%COMBO(DocumentType)%%>', $combo_DocumentType->HTML, $templateCode);
	$templateCode = str_replace('<%%COMBOTEXT(DocumentType)%%>', $combo_DocumentType->SelectedData, $templateCode);

	/* lookup fields array: 'lookup field name' => array('parent table name', 'lookup field caption') */
	$lookup_fields = array('VolunteerId' => array('Club_Volunteers', 'VolunteerId'), );
	foreach($lookup_fields as $luf => $ptfc) {
		$pt_perm = getTablePermissions($ptfc[0]);

		// process foreign key links
		if($pt_perm['view'] || $pt_perm['edit']) {
			$templateCode = str_replace("<%%PLINK({$luf})%%>", '<button type="button" class="btn btn-default view_parent hspacer-md" id="' . $ptfc[0] . '_view_parent" title="' . html_attr($Translation['View'] . ' ' . $ptfc[1]) . '"><i class="glyphicon glyphicon-eye-open"></i></button>', $templateCode);
		}

		// if user has insert permission to parent table of a lookup field, put an add new button
		if($pt_perm['insert'] && !$_REQUEST['Embedded']) {
			$templateCode = str_replace("<%%ADDNEW({$ptfc[0]})%%>", '<button type="button" class="btn btn-success add_new_parent hspacer-md" id="' . $ptfc[0] . '_add_new" title="' . html_attr($Translation['Add New'] . ' ' . $ptfc[1]) . '"><i class="glyphicon glyphicon-plus-sign"></i></button>', $templateCode);
		}
	}

	// process images
	$templateCode = str_replace('<%%UPLOADFILE(Id)%%>', '', $templateCode);
	$templateCode = str_replace('<%%UPLOADFILE(VolunteerId)%%>', '', $templateCode);
	$templateCode = str_replace('<%%UPLOADFILE(DocumentDate)%%>', '', $templateCode);
	$templateCode = str_replace('<%%UPLOADFILE(DocumentExpiryDate)%%>', '', $templateCode);
	$templateCode = str_replace('<%%UPLOADFILE(DocumentType)%%>', '', $templateCode);
	$templateCode = str_replace('<%%UPLOADFILE(DocumentLink)%%>', ($noUploads ? '' : "<div>{$Translation['upload image']}</div>" . '<i class="glyphicon glyphicon-remove text-danger clear-upload hidden"></i> <input type="file" name="DocumentLink" id="DocumentLink" data-filetypes="txt|doc|docx|docm|odt|pdf|rtf" data-maxsize="5120000">'), $templateCode);
	if($AllowUpdate && $row['DocumentLink'] != '') {
		$templateCode = str_replace('<%%REMOVEFILE(DocumentLink)%%>', '<br><input type="checkbox" name="DocumentLink_remove" id="DocumentLink_remove" value="1"> <label for="DocumentLink_remove" style="color: red; font-weight: bold;">'.$Translation['remove image'].'</label>', $templateCode);
	}else{
		$templateCode = str_replace('<%%REMOVEFILE(DocumentLink)%%>', '', $templateCode);
	}
	$templateCode = str_replace('<%%UPLOADFILE(Notes)%%>', '', $templateCode);

	// process values
	if($selected_id) {
		if( $dvprint) $templateCode = str_replace('<%%VALUE(Id)%%>', safe_html($urow['Id']), $templateCode);
		if(!$dvprint) $templateCode = str_replace('<%%VALUE(Id)%%>', html_attr($row['Id']), $templateCode);
		$templateCode = str_replace('<%%URLVALUE(Id)%%>', urlencode($urow['Id']), $templateCode);
		if( $dvprint) $templateCode = str_replace('<%%VALUE(VolunteerId)%%>', safe_html($urow['VolunteerId']), $templateCode);
		if(!$dvprint) $templateCode = str_replace('<%%VALUE(VolunteerId)%%>', html_attr($row['VolunteerId']), $templateCode);
		$templateCode = str_replace('<%%URLVALUE(VolunteerId)%%>', urlencode($urow['VolunteerId']), $templateCode);
		$templateCode = str_replace('<%%VALUE(DocumentDate)%%>', @date('d/m/Y', @strtotime(html_attr($row['DocumentDate']))), $templateCode);
		$templateCode = str_replace('<%%URLVALUE(DocumentDate)%%>', urlencode(@date('d/m/Y', @strtotime(html_attr($urow['DocumentDate'])))), $templateCode);
		$templateCode = str_replace('<%%VALUE(DocumentExpiryDate)%%>', @date('d/m/Y', @strtotime(html_attr($row['DocumentExpiryDate']))), $templateCode);
		$templateCode = str_replace('<%%URLVALUE(DocumentExpiryDate)%%>', urlencode(@date('d/m/Y', @strtotime(html_attr($urow['DocumentExpiryDate'])))), $templateCode);
		if( $dvprint) $templateCode = str_replace('<%%VALUE(DocumentType)%%>', safe_html($urow['DocumentType']), $templateCode);
		if(!$dvprint) $templateCode = str_replace('<%%VALUE(DocumentType)%%>', html_attr($row['DocumentType']), $templateCode);
		$templateCode = str_replace('<%%URLVALUE(DocumentType)%%>', urlencode($urow['DocumentType']), $templateCode);
		if( $dvprint) $templateCode = str_replace('<%%VALUE(DocumentLink)%%>', safe_html($urow['DocumentLink']), $templateCode);
		if(!$dvprint) $templateCode = str_replace('<%%VALUE(DocumentLink)%%>', html_attr($row['DocumentLink']), $templateCode);
		$templateCode = str_replace('<%%URLVALUE(DocumentLink)%%>', urlencode($urow['DocumentLink']), $templateCode);
		if($AllowUpdate || $AllowInsert) {
			$templateCode = str_replace('<%%HTMLAREA(Notes)%%>', '<textarea name="Notes" id="Notes" rows="5">' . html_attr($row['Notes']) . '</textarea>', $templateCode);
		}else{
			$templateCode = str_replace('<%%HTMLAREA(Notes)%%>', '<div id="Notes" class="form-control-static">' . $row['Notes'] . '</div>', $templateCode);
		}
		$templateCode = str_replace('<%%VALUE(Notes)%%>', nl2br($row['Notes']), $templateCode);
		$templateCode = str_replace('<%%URLVALUE(Notes)%%>', urlencode($urow['Notes']), $templateCode);
	}else{
		$templateCode = str_replace('<%%VALUE(Id)%%>', '', $templateCode);
		$templateCode = str_replace('<%%URLVALUE(Id)%%>', urlencode(''), $templateCode);
		$templateCode = str_replace('<%%VALUE(VolunteerId)%%>', '', $templateCode);
		$templateCode = str_replace('<%%URLVALUE(VolunteerId)%%>', urlencode(''), $templateCode);
		$templateCode = str_replace('<%%VALUE(DocumentDate)%%>', '', $templateCode);
		$templateCode = str_replace('<%%URLVALUE(DocumentDate)%%>', urlencode(''), $templateCode);
		$templateCode = str_replace('<%%VALUE(DocumentExpiryDate)%%>', '', $templateCode);
		$templateCode = str_replace('<%%URLVALUE(DocumentExpiryDate)%%>', urlencode(''), $templateCode);
		$templateCode = str_replace('<%%VALUE(DocumentType)%%>', '', $templateCode);
		$templateCode = str_replace('<%%URLVALUE(DocumentType)%%>', urlencode(''), $templateCode);
		$templateCode = str_replace('<%%VALUE(DocumentLink)%%>', '', $templateCode);
		$templateCode = str_replace('<%%URLVALUE(DocumentLink)%%>', urlencode(''), $templateCode);
		$templateCode = str_replace('<%%HTMLAREA(Notes)%%>', '<textarea name="Notes" id="Notes" rows="5"></textarea>', $templateCode);
	}

	// process translations
	foreach($Translation as $symbol=>$trans) {
		$templateCode = str_replace("<%%TRANSLATION($symbol)%%>", $trans, $templateCode);
	}

	// clear scrap
	$templateCode = str_replace('<%%', '<!-- ', $templateCode);
	$templateCode = str_replace('%%>', ' -->', $templateCode);

	// hide links to inaccessible tables
	if($_REQUEST['dvprint_x'] == '') {
		$templateCode .= "\n\n<script>\$j(function() {\n";
		$arrTables = getTableList();
		foreach($arrTables as $name => $caption) {
			$templateCode .= "\t\$j('#{$name}_link').removeClass('hidden');\n";
			$templateCode .= "\t\$j('#xs_{$name}_link').removeClass('hidden');\n";
		}

		$templateCode .= $jsReadOnly;
		$templateCode .= $jsEditable;

		if(!$selected_id) {
			$templateCode.="\n\tif(document.getElementById('DocumentLinkEdit')) { document.getElementById('DocumentLinkEdit').style.display='inline'; }";
			$templateCode.="\n\tif(document.getElementById('DocumentLinkEditLink')) { document.getElementById('DocumentLinkEditLink').style.display='none'; }";
		}

		$templateCode.="\n});</script>\n";
	}

	// ajaxed auto-fill fields
	$templateCode .= '<script>';
	$templateCode .= '$j(function() {';


	$templateCode.="});";
	$templateCode.="</script>";
	$templateCode .= $lookups;

	// handle enforced parent values for read-only lookup fields

	// don't include blank images in lightbox gallery
	$templateCode = preg_replace('/blank.gif" data-lightbox=".*?"/', 'blank.gif"', $templateCode);

	// don't display empty email links
	$templateCode=preg_replace('/<a .*?href="mailto:".*?<\/a>/', '', $templateCode);

	/* default field values */
	$rdata = $jdata = get_defaults('Club_VolunteerDocumentation');
	if($selected_id) {
		$jdata = get_joined_record('Club_VolunteerDocumentation', $selected_id);
		if($jdata === false) $jdata = get_defaults('Club_VolunteerDocumentation');
		$rdata = $row;
	}
	$templateCode .= loadView('Club_VolunteerDocumentation-ajax-cache', array('rdata' => $rdata, 'jdata' => $jdata));

	// hook: Club_VolunteerDocumentation_dv
	if(function_exists('Club_VolunteerDocumentation_dv')) {
		$args=array();
		Club_VolunteerDocumentation_dv(($selected_id ? $selected_id : FALSE), getMemberInfo(), $templateCode, $args);
	}

	return $templateCode;
}
?>