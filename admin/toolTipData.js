var FiltersEnabled = 0; // if your not going to use transitions or filters in any of the tips set this to 0
var spacer="&nbsp; &nbsp; &nbsp; ";

// email notifications to admin
notifyAdminNewMembers0Tip=["", spacer+"No email notifications to admin."];
notifyAdminNewMembers1Tip=["", spacer+"Notify admin only when a new member is waiting for approval."];
notifyAdminNewMembers2Tip=["", spacer+"Notify admin for all new sign-ups."];

// visitorSignup
visitorSignup0Tip=["", spacer+"If this option is selected, visitors will not be able to join this group unless the admin manually moves them to this group from the admin area."];
visitorSignup1Tip=["", spacer+"If this option is selected, visitors can join this group but will not be able to sign in unless the admin approves them from the admin area."];
visitorSignup2Tip=["", spacer+"If this option is selected, visitors can join this group and will be able to sign in instantly with no need for admin approval."];

// Club_Members table
Club_Members_addTip=["",spacer+"This option allows all members of the group to add records to the 'Members' table. A member who adds a record to the table becomes the 'owner' of that record."];

Club_Members_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'Members' table."];
Club_Members_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'Members' table."];
Club_Members_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'Members' table."];
Club_Members_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'Members' table."];

Club_Members_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'Members' table."];
Club_Members_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'Members' table."];
Club_Members_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'Members' table."];
Club_Members_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'Members' table, regardless of their owner."];

Club_Members_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'Members' table."];
Club_Members_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'Members' table."];
Club_Members_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'Members' table."];
Club_Members_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'Members' table."];

// Club_Volunteers table
Club_Volunteers_addTip=["",spacer+"This option allows all members of the group to add records to the 'Volunteers' table. A member who adds a record to the table becomes the 'owner' of that record."];

Club_Volunteers_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'Volunteers' table."];
Club_Volunteers_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'Volunteers' table."];
Club_Volunteers_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'Volunteers' table."];
Club_Volunteers_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'Volunteers' table."];

Club_Volunteers_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'Volunteers' table."];
Club_Volunteers_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'Volunteers' table."];
Club_Volunteers_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'Volunteers' table."];
Club_Volunteers_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'Volunteers' table, regardless of their owner."];

Club_Volunteers_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'Volunteers' table."];
Club_Volunteers_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'Volunteers' table."];
Club_Volunteers_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'Volunteers' table."];
Club_Volunteers_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'Volunteers' table."];

// Club_SessionDates table
Club_SessionDates_addTip=["",spacer+"This option allows all members of the group to add records to the 'Sessions' table. A member who adds a record to the table becomes the 'owner' of that record."];

Club_SessionDates_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'Sessions' table."];
Club_SessionDates_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'Sessions' table."];
Club_SessionDates_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'Sessions' table."];
Club_SessionDates_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'Sessions' table."];

Club_SessionDates_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'Sessions' table."];
Club_SessionDates_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'Sessions' table."];
Club_SessionDates_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'Sessions' table."];
Club_SessionDates_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'Sessions' table, regardless of their owner."];

Club_SessionDates_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'Sessions' table."];
Club_SessionDates_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'Sessions' table."];
Club_SessionDates_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'Sessions' table."];
Club_SessionDates_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'Sessions' table."];

// Club_Attendance table
Club_Attendance_addTip=["",spacer+"This option allows all members of the group to add records to the 'Attendance' table. A member who adds a record to the table becomes the 'owner' of that record."];

Club_Attendance_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'Attendance' table."];
Club_Attendance_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'Attendance' table."];
Club_Attendance_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'Attendance' table."];
Club_Attendance_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'Attendance' table."];

Club_Attendance_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'Attendance' table."];
Club_Attendance_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'Attendance' table."];
Club_Attendance_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'Attendance' table."];
Club_Attendance_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'Attendance' table, regardless of their owner."];

Club_Attendance_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'Attendance' table."];
Club_Attendance_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'Attendance' table."];
Club_Attendance_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'Attendance' table."];
Club_Attendance_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'Attendance' table."];

// Club_VolunteerAttendance table
Club_VolunteerAttendance_addTip=["",spacer+"This option allows all members of the group to add records to the 'Volunteer Attendance' table. A member who adds a record to the table becomes the 'owner' of that record."];

Club_VolunteerAttendance_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'Volunteer Attendance' table."];
Club_VolunteerAttendance_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'Volunteer Attendance' table."];
Club_VolunteerAttendance_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'Volunteer Attendance' table."];
Club_VolunteerAttendance_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'Volunteer Attendance' table."];

Club_VolunteerAttendance_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'Volunteer Attendance' table."];
Club_VolunteerAttendance_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'Volunteer Attendance' table."];
Club_VolunteerAttendance_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'Volunteer Attendance' table."];
Club_VolunteerAttendance_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'Volunteer Attendance' table, regardless of their owner."];

Club_VolunteerAttendance_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'Volunteer Attendance' table."];
Club_VolunteerAttendance_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'Volunteer Attendance' table."];
Club_VolunteerAttendance_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'Volunteer Attendance' table."];
Club_VolunteerAttendance_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'Volunteer Attendance' table."];

// Club_VolunteerDocumentation table
Club_VolunteerDocumentation_addTip=["",spacer+"This option allows all members of the group to add records to the 'Volunteer Documentation' table. A member who adds a record to the table becomes the 'owner' of that record."];

Club_VolunteerDocumentation_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'Volunteer Documentation' table."];
Club_VolunteerDocumentation_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'Volunteer Documentation' table."];
Club_VolunteerDocumentation_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'Volunteer Documentation' table."];
Club_VolunteerDocumentation_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'Volunteer Documentation' table."];

Club_VolunteerDocumentation_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'Volunteer Documentation' table."];
Club_VolunteerDocumentation_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'Volunteer Documentation' table."];
Club_VolunteerDocumentation_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'Volunteer Documentation' table."];
Club_VolunteerDocumentation_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'Volunteer Documentation' table, regardless of their owner."];

Club_VolunteerDocumentation_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'Volunteer Documentation' table."];
Club_VolunteerDocumentation_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'Volunteer Documentation' table."];
Club_VolunteerDocumentation_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'Volunteer Documentation' table."];
Club_VolunteerDocumentation_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'Volunteer Documentation' table."];

// Club_AgeCats table
Club_AgeCats_addTip=["",spacer+"This option allows all members of the group to add records to the 'Age Categories' table. A member who adds a record to the table becomes the 'owner' of that record."];

Club_AgeCats_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'Age Categories' table."];
Club_AgeCats_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'Age Categories' table."];
Club_AgeCats_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'Age Categories' table."];
Club_AgeCats_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'Age Categories' table."];

Club_AgeCats_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'Age Categories' table."];
Club_AgeCats_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'Age Categories' table."];
Club_AgeCats_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'Age Categories' table."];
Club_AgeCats_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'Age Categories' table, regardless of their owner."];

Club_AgeCats_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'Age Categories' table."];
Club_AgeCats_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'Age Categories' table."];
Club_AgeCats_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'Age Categories' table."];
Club_AgeCats_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'Age Categories' table."];

// Club_Events table
Club_Events_addTip=["",spacer+"This option allows all members of the group to add records to the 'Club Events' table. A member who adds a record to the table becomes the 'owner' of that record."];

Club_Events_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'Club Events' table."];
Club_Events_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'Club Events' table."];
Club_Events_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'Club Events' table."];
Club_Events_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'Club Events' table."];

Club_Events_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'Club Events' table."];
Club_Events_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'Club Events' table."];
Club_Events_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'Club Events' table."];
Club_Events_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'Club Events' table, regardless of their owner."];

Club_Events_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'Club Events' table."];
Club_Events_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'Club Events' table."];
Club_Events_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'Club Events' table."];
Club_Events_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'Club Events' table."];

// Club_EventEntrants table
Club_EventEntrants_addTip=["",spacer+"This option allows all members of the group to add records to the 'Club Event Entrants' table. A member who adds a record to the table becomes the 'owner' of that record."];

Club_EventEntrants_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'Club Event Entrants' table."];
Club_EventEntrants_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'Club Event Entrants' table."];
Club_EventEntrants_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'Club Event Entrants' table."];
Club_EventEntrants_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'Club Event Entrants' table."];

Club_EventEntrants_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'Club Event Entrants' table."];
Club_EventEntrants_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'Club Event Entrants' table."];
Club_EventEntrants_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'Club Event Entrants' table."];
Club_EventEntrants_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'Club Event Entrants' table, regardless of their owner."];

Club_EventEntrants_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'Club Event Entrants' table."];
Club_EventEntrants_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'Club Event Entrants' table."];
Club_EventEntrants_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'Club Event Entrants' table."];
Club_EventEntrants_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'Club Event Entrants' table."];

/*
	Style syntax:
	-------------
	[TitleColor,TextColor,TitleBgColor,TextBgColor,TitleBgImag,TextBgImag,TitleTextAlign,
	TextTextAlign,TitleFontFace,TextFontFace, TipPosition, StickyStyle, TitleFontSize,
	TextFontSize, Width, Height, BorderSize, PadTextArea, CoordinateX , CoordinateY,
	TransitionNumber, TransitionDuration, TransparencyLevel ,ShadowType, ShadowColor]

*/

toolTipStyle=["white","#00008B","#000099","#E6E6FA","","images/helpBg.gif","","","","\"Trebuchet MS\", sans-serif","","","","3",400,"",1,2,10,10,51,1,0,"",""];

applyCssFilter();
