# club-manager

Cycling Club management tool

## Getting started

club-manager is a free web/mobile enabled tool to help manage club members, volunteers, events and attendance.

The source code can be deployed to any sub folder in a host's web root (i.e. htdocs/club-manager)

Before you begin, create a mysql database and a user for club-manager. The datbase user should have dbo privileges



## Upload the files

- [ ] Upload this entire repo to your chosen subfolder
- [ ] Access the subfolder via a browser, for example http://wwww.your-site/club-manager where your site is your domain and club-manager is your chosen path
- [ ] Follow the on-screen instructions to enter: 
- - [ ] Your database name
- - [ ] The hostname or ip address of your database
- - [ ] The database user name
- - [ ] The database password
- - [ ] An admin user name and password for the secure functions in the tool (I suggest "administrator" as the user)
- - [ ] An email address for the admin user in case of admin user password resets

## The Interface

- [ ] The user interface is accessed by signon, and the admin functions will allow user creation
- [ ] User permissions can be added to the different tables as required
- [ ] The user area presents two initial tables:
- Members
- Attendance
- [ ] You will notice that other menu options are collapsed, but can be extended at will to show:
- Volunteers:
- - Manage Vomuteer Details
- - Manage Volunteer Attendance
- - Manage Volunteer Documentation
- Sessions and Events
- - Manage Session Details
- - Manage Age Categories for Youth/Junior riders
- - Manage Events
- - Manage Attendance to Events

## The Database

- [ ] All tables have a parent/child hierarchy
- [ ] For instance, once you have created say members and sessions, you can start to add relationships between members and their attendance at sessions
- [ ] The same is true for events, however the intention of registering members to events is to build and manage start lists and results
- [ ] The links are made available from parent records to connected child records throughout the application 

***

# Features

Perhaps one of the most useful features you will use is Filters. This will allow you to create custom select or omits over the data and save them to custom your views in many ways. As part of the admin features, this works well also with user groups so that access to data can be partitioned easily.

Tables support CSV download from the user interface, and CSV import is provided via the Admin area. Import can be tricky if you are unsure what you are doing, so the admin area also provides a db backup utility under Admin > Utilities

# Updates

Source code is provided as is, and no changes are planned. If there is a feature that is required, or an extension to a table, please contact dg@fxa.co.uk 
All updates can be deployed in situ without affecting the database
