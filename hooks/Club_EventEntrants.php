<?php
	// For help on using hooks, please refer to https://bigprof.com/appgini/help/working-with-generated-web-database-application/hooks

	function Club_EventEntrants_init(&$options, $memberInfo, &$args) {

		return TRUE;
	}

	function Club_EventEntrants_header($contentType, $memberInfo, &$args) {
		$header='';

		switch($contentType) {
			case 'tableview':
				$header='';
				break;

			case 'detailview':
				$header='';
				break;

			case 'tableview+detailview':
				$header='';
				break;

			case 'print-tableview':
				$header='';
				break;

			case 'print-detailview':
				$header='';
				break;

			case 'filters':
				$header='';
				break;
		}

		return $header;
	}

	function Club_EventEntrants_footer($contentType, $memberInfo, &$args) {
		$footer='';

		switch($contentType) {
			case 'tableview':
				$footer='';
				break;

			case 'detailview':
				$footer='';
				break;

			case 'tableview+detailview':
				$footer='';
				break;

			case 'print-tableview':
				$footer='';
				break;

			case 'print-detailview':
				$footer='';
				break;

			case 'filters':
				$footer='';
				break;
		}

		return $footer;
	}

	function Club_EventEntrants_before_insert(&$data, $memberInfo, &$args) {

		return TRUE;
	}

	function Club_EventEntrants_after_insert($data, $memberInfo, &$args) {

		return TRUE;
	}

	function Club_EventEntrants_before_update(&$data, $memberInfo, &$args) {

		return TRUE;
	}

	function Club_EventEntrants_after_update($data, $memberInfo, &$args) {

		return TRUE;
	}

	function Club_EventEntrants_before_delete($selectedID, &$skipChecks, $memberInfo, &$args) {

		return TRUE;
	}

	function Club_EventEntrants_after_delete($selectedID, $memberInfo, &$args) {

	}

	function Club_EventEntrants_dv($selectedID, $memberInfo, &$html, &$args) {

	}

	function Club_EventEntrants_csv($query, $memberInfo, &$args) {

		return $query;
	}
	function Club_EventEntrants_batch_actions(&$args) {

		return array();
	}
