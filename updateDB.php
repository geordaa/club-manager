<?php
	// check this file's MD5 to make sure it wasn't called before
	$prevMD5=@implode('', @file(dirname(__FILE__).'/setup.md5'));
	$thisMD5=md5(@implode('', @file("./updateDB.php")));
	if($thisMD5==$prevMD5) {
		$setupAlreadyRun=true;
	}else{
		// set up tables
		if(!isset($silent)) {
			$silent=true;
		}

		// set up tables
		setupTable('Club_Members', "create table if not exists `Club_Members` (   `Id` INT unsigned not null auto_increment , primary key (`Id`), `MemberID` INT null , unique `MemberID_unique` (`MemberID`), `FirstName` VARCHAR(40) null , `LastName` VARCHAR(40) null , `DateStarted` DATE null , `YearOfBirth` DATE null , `Age` VARCHAR(40) null , `Gender` VARCHAR(40) null , `Licence` MEDIUMINT(7) null , `Active` VARCHAR(40) null , `Bikeability` VARCHAR(40) null , `EmergencyContact1` VARCHAR(40) not null , `EmergencyContact2` VARCHAR(40) null , `MembershipType` TEXT null ) CHARSET utf8", $silent);
		setupTable('Club_Volunteers', "create table if not exists `Club_Volunteers` (   `Id` INT unsigned not null auto_increment , primary key (`Id`), `FirstName` VARCHAR(40) null , `LastName` VARCHAR(40) null , `Started` DATE not null , `Under18` VARCHAR(40) null , `Roles` TEXT null , `Image` VARCHAR(40) null , `Email` VARCHAR(80) null ) CHARSET utf8", $silent);
		setupTable('Club_SessionDates', "create table if not exists `Club_SessionDates` (   `Id` INT unsigned not null auto_increment , primary key (`Id`), `Date` DATE null , `Type` VARCHAR(40) null , `SubType` TEXT null , `Notes` TEXT null ) CHARSET utf8", $silent);
		setupTable('Club_Attendance', "create table if not exists `Club_Attendance` (   `Id` INT unsigned not null auto_increment , primary key (`Id`), `Date` INT unsigned null default '1' , `MemberID` INT unsigned null , `Notes` TEXT null , `KnownAbsence` VARCHAR(40) null , `Volunteer` VARCHAR(40) null ) CHARSET utf8", $silent);
		setupIndexes('Club_Attendance', array('Date','MemberID'));
		setupTable('Club_VolunteerAttendance', "create table if not exists `Club_VolunteerAttendance` (   `Id` INT unsigned not null auto_increment , primary key (`Id`), `Date` INT unsigned null default '1' , `VolunteerId` INT unsigned null , `Notes` TEXT null ) CHARSET utf8", $silent);
		setupIndexes('Club_VolunteerAttendance', array('Date','VolunteerId'));
		setupTable('Club_VolunteerDocumentation', "create table if not exists `Club_VolunteerDocumentation` (   `Id` INT unsigned not null auto_increment , primary key (`Id`), `VolunteerId` INT unsigned null , `DocumentDate` DATE null , `DocumentExpiryDate` DATE null , `DocumentType` VARCHAR(40) null , `DocumentLink` VARCHAR(40) null , `Notes` TEXT null ) CHARSET utf8", $silent);
		setupIndexes('Club_VolunteerDocumentation', array('VolunteerId'));
		setupTable('Club_AgeCats', "create table if not exists `Club_AgeCats` (   `Id` INT unsigned not null auto_increment , primary key (`Id`), `Cat` VARCHAR(1) null , `Description` VARCHAR(40) null , `StartYearOfBirth` INT(4) null , `EndYearOfBirth` INT(4) null , `GearRoad` VARCHAR(40) null , `GearTrack` VARCHAR(40) null , `GearRoller` VARCHAR(40) null ) CHARSET utf8", $silent);
		setupTable('Club_Events', "create table if not exists `Club_Events` (   `Id` INT unsigned not null auto_increment , primary key (`Id`), `Date` DATE not null , `Title` VARCHAR(40) not null , `Description` TEXT not null , `StartTime` TIME not null , `EndTime` TIME not null , `EventAddress` VARCHAR(40) not null , `Map` VARCHAR(200) null ) CHARSET utf8", $silent);
		setupTable('Club_EventEntrants', "create table if not exists `Club_EventEntrants` (   `Id` INT unsigned not null auto_increment , primary key (`Id`), `Number` INT not null , `Event` INT unsigned not null default '1' , `Entrant` INT unsigned null , `MemberID` INT unsigned null , `Licence` INT unsigned null , `Emergency1` INT unsigned null , `Emergency2` INT unsigned null , `DateOfBirth` INT unsigned null , `Age` INT unsigned null , `Gender` INT unsigned null , `StartTime` TIME null , `EndTime` TIME null , `Position` INT null ) CHARSET utf8", $silent);
		setupIndexes('Club_EventEntrants', array('Event','Entrant'));


		// save MD5
		if($fp=@fopen(dirname(__FILE__).'/setup.md5', 'w')) {
			fwrite($fp, $thisMD5);
			fclose($fp);
		}
	}


	function setupIndexes($tableName, $arrFields) {
		if(!is_array($arrFields)) {
			return false;
		}

		foreach($arrFields as $fieldName) {
			if(!$res=@db_query("SHOW COLUMNS FROM `$tableName` like '$fieldName'")) {
				continue;
			}
			if(!$row=@db_fetch_assoc($res)) {
				continue;
			}
			if($row['Key']=='') {
				@db_query("ALTER TABLE `$tableName` ADD INDEX `$fieldName` (`$fieldName`)");
			}
		}
	}


	function setupTable($tableName, $createSQL='', $silent=true, $arrAlter='') {
		global $Translation;
		ob_start();

		echo '<div style="padding: 5px; border-bottom:solid 1px silver; font-family: verdana, arial; font-size: 10px;">';

		// is there a table rename query?
		if(is_array($arrAlter)) {
			$matches=array();
			if(preg_match("/ALTER TABLE `(.*)` RENAME `$tableName`/", $arrAlter[0], $matches)) {
				$oldTableName=$matches[1];
			}
		}

		if($res=@db_query("select count(1) from `$tableName`")) { // table already exists
			if($row = @db_fetch_array($res)) {
				echo str_replace("<TableName>", $tableName, str_replace("<NumRecords>", $row[0],$Translation["table exists"]));
				if(is_array($arrAlter)) {
					echo '<br>';
					foreach($arrAlter as $alter) {
						if($alter!='') {
							echo "$alter ... ";
							if(!@db_query($alter)) {
								echo '<span class="label label-danger">' . $Translation['failed'] . '</span>';
								echo '<div class="text-danger">' . $Translation['mysql said'] . ' ' . db_error(db_link()) . '</div>';
							}else{
								echo '<span class="label label-success">' . $Translation['ok'] . '</span>';
							}
						}
					}
				}else{
					echo $Translation["table uptodate"];
				}
			}else{
				echo str_replace("<TableName>", $tableName, $Translation["couldnt count"]);
			}
		}else{ // given tableName doesn't exist

			if($oldTableName!='') { // if we have a table rename query
				if($ro=@db_query("select count(1) from `$oldTableName`")) { // if old table exists, rename it.
					$renameQuery=array_shift($arrAlter); // get and remove rename query

					echo "$renameQuery ... ";
					if(!@db_query($renameQuery)) {
						echo '<span class="label label-danger">' . $Translation['failed'] . '</span>';
						echo '<div class="text-danger">' . $Translation['mysql said'] . ' ' . db_error(db_link()) . '</div>';
					}else{
						echo '<span class="label label-success">' . $Translation['ok'] . '</span>';
					}

					if(is_array($arrAlter)) setupTable($tableName, $createSQL, false, $arrAlter); // execute Alter queries on renamed table ...
				}else{ // if old tableName doesn't exist (nor the new one since we're here), then just create the table.
					setupTable($tableName, $createSQL, false); // no Alter queries passed ...
				}
			}else{ // tableName doesn't exist and no rename, so just create the table
				echo str_replace("<TableName>", $tableName, $Translation["creating table"]);
				if(!@db_query($createSQL)) {
					echo '<span class="label label-danger">' . $Translation['failed'] . '</span>';
					echo '<div class="text-danger">' . $Translation['mysql said'] . db_error(db_link()) . '</div>';
				}else{
					echo '<span class="label label-success">' . $Translation['ok'] . '</span>';
				}
			}
		}

		echo "</div>";

		$out=ob_get_contents();
		ob_end_clean();
		if(!$silent) {
			echo $out;
		}
	}
?>