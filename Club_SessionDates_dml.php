<?php

// Data functions (insert, update, delete, form) for table Club_SessionDates

// This script and data application were generated by AppGini 5.82
// Download AppGini for free from https://bigprof.com/appgini/download/

function Club_SessionDates_insert() {
	global $Translation;

	// mm: can member insert record?
	$arrPerm = getTablePermissions('Club_SessionDates');
	if(!$arrPerm[1]) return false;

	$data = array();
	$data['Date'] = intval($_REQUEST['DateYear']) . '-' . intval($_REQUEST['DateMonth']) . '-' . intval($_REQUEST['DateDay']);
	$data['Date'] = parseMySQLDate($data['Date'], '1');
	$data['Type'] = $_REQUEST['Type'];
		if($data['Type'] == empty_lookup_value) { $data['Type'] = ''; }
	$data['SubType'] = '';
	if(is_array($_REQUEST['SubType'])) {
		$MultipleSeparator = ', ';
		$data['SubType'] = implode($MultipleSeparator, $_REQUEST['SubType']);
	}
	$data['Notes'] = $_REQUEST['Notes'];
		if($data['Notes'] == empty_lookup_value) { $data['Notes'] = ''; }

	// hook: Club_SessionDates_before_insert
	if(function_exists('Club_SessionDates_before_insert')) {
		$args = array();
		if(!Club_SessionDates_before_insert($data, getMemberInfo(), $args)) { return false; }
	}

	$error = '';
	// set empty fields to NULL
	$data = array_map(function($v) { return ($v === '' ? NULL : $v); }, $data);
	insert('Club_SessionDates', backtick_keys_once($data), $error);
	if($error)
		die("{$error}<br><a href=\"#\" onclick=\"history.go(-1);\">{$Translation['< back']}</a>");

	$recID = db_insert_id(db_link());

	// hook: Club_SessionDates_after_insert
	if(function_exists('Club_SessionDates_after_insert')) {
		$res = sql("select * from `Club_SessionDates` where `Id`='" . makeSafe($recID, false) . "' limit 1", $eo);
		if($row = db_fetch_assoc($res)) {
			$data = array_map('makeSafe', $row);
		}
		$data['selectedID'] = makeSafe($recID, false);
		$args=array();
		if(!Club_SessionDates_after_insert($data, getMemberInfo(), $args)) { return $recID; }
	}

	// mm: save ownership data
	set_record_owner('Club_SessionDates', $recID, getLoggedMemberID());

	// if this record is a copy of another record, copy children if applicable
	if(!empty($_REQUEST['SelectedID'])) Club_SessionDates_copy_children($recID, $_REQUEST['SelectedID']);

	return $recID;
}

function Club_SessionDates_copy_children($destination_id, $source_id) {
	global $Translation;
	$requests = array(); // array of curl handlers for launching insert requests
	$eo = array('silentErrors' => true);
	$uploads_dir = realpath(dirname(__FILE__) . '/../' . $Translation['ImageFolder']);
	$safe_sid = makeSafe($source_id);

	// launch requests, asynchronously
	curl_batch($requests);
}

function Club_SessionDates_delete($selected_id, $AllowDeleteOfParents=false, $skipChecks=false) {
	// insure referential integrity ...
	global $Translation;
	$selected_id=makeSafe($selected_id);

	// mm: can member delete record?
	$arrPerm=getTablePermissions('Club_SessionDates');
	$ownerGroupID=sqlValue("select groupID from membership_userrecords where tableName='Club_SessionDates' and pkValue='$selected_id'");
	$ownerMemberID=sqlValue("select lcase(memberID) from membership_userrecords where tableName='Club_SessionDates' and pkValue='$selected_id'");
	if(($arrPerm[4]==1 && $ownerMemberID==getLoggedMemberID()) || ($arrPerm[4]==2 && $ownerGroupID==getLoggedGroupID()) || $arrPerm[4]==3) { // allow delete?
		// delete allowed, so continue ...
	}else{
		return $Translation['You don\'t have enough permissions to delete this record'];
	}

	// hook: Club_SessionDates_before_delete
	if(function_exists('Club_SessionDates_before_delete')) {
		$args=array();
		if(!Club_SessionDates_before_delete($selected_id, $skipChecks, getMemberInfo(), $args))
			return $Translation['Couldn\'t delete this record'];
	}

	// child table: Club_Attendance
	$res = sql("select `Id` from `Club_SessionDates` where `Id`='$selected_id'", $eo);
	$Id = db_fetch_row($res);
	$rires = sql("select count(1) from `Club_Attendance` where `Date`='".addslashes($Id[0])."'", $eo);
	$rirow = db_fetch_row($rires);
	if($rirow[0] && !$AllowDeleteOfParents && !$skipChecks) {
		$RetMsg = $Translation["couldn't delete"];
		$RetMsg = str_replace("<RelatedRecords>", $rirow[0], $RetMsg);
		$RetMsg = str_replace("<TableName>", "Club_Attendance", $RetMsg);
		return $RetMsg;
	}elseif($rirow[0] && $AllowDeleteOfParents && !$skipChecks) {
		$RetMsg = $Translation["confirm delete"];
		$RetMsg = str_replace("<RelatedRecords>", $rirow[0], $RetMsg);
		$RetMsg = str_replace("<TableName>", "Club_Attendance", $RetMsg);
		$RetMsg = str_replace("<Delete>", "<input type=\"button\" class=\"button\" value=\"".$Translation['yes']."\" onClick=\"window.location='Club_SessionDates_view.php?SelectedID=".urlencode($selected_id)."&delete_x=1&confirmed=1';\">", $RetMsg);
		$RetMsg = str_replace("<Cancel>", "<input type=\"button\" class=\"button\" value=\"".$Translation['no']."\" onClick=\"window.location='Club_SessionDates_view.php?SelectedID=".urlencode($selected_id)."';\">", $RetMsg);
		return $RetMsg;
	}

	// child table: Club_VolunteerAttendance
	$res = sql("select `Id` from `Club_SessionDates` where `Id`='$selected_id'", $eo);
	$Id = db_fetch_row($res);
	$rires = sql("select count(1) from `Club_VolunteerAttendance` where `Date`='".addslashes($Id[0])."'", $eo);
	$rirow = db_fetch_row($rires);
	if($rirow[0] && !$AllowDeleteOfParents && !$skipChecks) {
		$RetMsg = $Translation["couldn't delete"];
		$RetMsg = str_replace("<RelatedRecords>", $rirow[0], $RetMsg);
		$RetMsg = str_replace("<TableName>", "Club_VolunteerAttendance", $RetMsg);
		return $RetMsg;
	}elseif($rirow[0] && $AllowDeleteOfParents && !$skipChecks) {
		$RetMsg = $Translation["confirm delete"];
		$RetMsg = str_replace("<RelatedRecords>", $rirow[0], $RetMsg);
		$RetMsg = str_replace("<TableName>", "Club_VolunteerAttendance", $RetMsg);
		$RetMsg = str_replace("<Delete>", "<input type=\"button\" class=\"button\" value=\"".$Translation['yes']."\" onClick=\"window.location='Club_SessionDates_view.php?SelectedID=".urlencode($selected_id)."&delete_x=1&confirmed=1';\">", $RetMsg);
		$RetMsg = str_replace("<Cancel>", "<input type=\"button\" class=\"button\" value=\"".$Translation['no']."\" onClick=\"window.location='Club_SessionDates_view.php?SelectedID=".urlencode($selected_id)."';\">", $RetMsg);
		return $RetMsg;
	}

	sql("delete from `Club_SessionDates` where `Id`='$selected_id'", $eo);

	// hook: Club_SessionDates_after_delete
	if(function_exists('Club_SessionDates_after_delete')) {
		$args=array();
		Club_SessionDates_after_delete($selected_id, getMemberInfo(), $args);
	}

	// mm: delete ownership data
	sql("delete from membership_userrecords where tableName='Club_SessionDates' and pkValue='$selected_id'", $eo);
}

function Club_SessionDates_update($selected_id) {
	global $Translation;

	// mm: can member edit record?
	$arrPerm=getTablePermissions('Club_SessionDates');
	$ownerGroupID=sqlValue("select groupID from membership_userrecords where tableName='Club_SessionDates' and pkValue='".makeSafe($selected_id)."'");
	$ownerMemberID=sqlValue("select lcase(memberID) from membership_userrecords where tableName='Club_SessionDates' and pkValue='".makeSafe($selected_id)."'");
	if(($arrPerm[3]==1 && $ownerMemberID==getLoggedMemberID()) || ($arrPerm[3]==2 && $ownerGroupID==getLoggedGroupID()) || $arrPerm[3]==3) { // allow update?
		// update allowed, so continue ...
	}else{
		return false;
	}

	$data['Date'] = intval($_REQUEST['DateYear']) . '-' . intval($_REQUEST['DateMonth']) . '-' . intval($_REQUEST['DateDay']);
	$data['Date'] = parseMySQLDate($data['Date'], '1');
	$data['Type'] = makeSafe($_REQUEST['Type']);
		if($data['Type'] == empty_lookup_value) { $data['Type'] = ''; }
	if(is_array($_REQUEST['SubType'])) {
		$MultipleSeparator = ', ';
		foreach($_REQUEST['SubType'] as $k => $v)
			$data['SubType'] .= makeSafe($v) . $MultipleSeparator;
		$data['SubType']=substr($data['SubType'], 0, -1 * strlen($MultipleSeparator));
	}else{
		$data['SubType']='';
	}
	$data['Notes'] = makeSafe($_REQUEST['Notes']);
		if($data['Notes'] == empty_lookup_value) { $data['Notes'] = ''; }
	$data['selectedID'] = makeSafe($selected_id);

	// hook: Club_SessionDates_before_update
	if(function_exists('Club_SessionDates_before_update')) {
		$args = array();
		if(!Club_SessionDates_before_update($data, getMemberInfo(), $args)) { return false; }
	}

	$o = array('silentErrors' => true);
	sql('update `Club_SessionDates` set       `Date`=' . (($data['Date'] !== '' && $data['Date'] !== NULL) ? "'{$data['Date']}'" : 'NULL') . ', `Type`=' . (($data['Type'] !== '' && $data['Type'] !== NULL) ? "'{$data['Type']}'" : 'NULL') . ', `SubType`=' . (($data['SubType'] !== '' && $data['SubType'] !== NULL) ? "'{$data['SubType']}'" : 'NULL') . ', `Notes`=' . (($data['Notes'] !== '' && $data['Notes'] !== NULL) ? "'{$data['Notes']}'" : 'NULL') . " where `Id`='".makeSafe($selected_id)."'", $o);
	if($o['error']!='') {
		echo $o['error'];
		echo '<a href="Club_SessionDates_view.php?SelectedID='.urlencode($selected_id)."\">{$Translation['< back']}</a>";
		exit;
	}


	// hook: Club_SessionDates_after_update
	if(function_exists('Club_SessionDates_after_update')) {
		$res = sql("SELECT * FROM `Club_SessionDates` WHERE `Id`='{$data['selectedID']}' LIMIT 1", $eo);
		if($row = db_fetch_assoc($res)) {
			$data = array_map('makeSafe', $row);
		}
		$data['selectedID'] = $data['Id'];
		$args = array();
		if(!Club_SessionDates_after_update($data, getMemberInfo(), $args)) { return; }
	}

	// mm: update ownership data
	sql("update `membership_userrecords` set `dateUpdated`='" . time() . "' where `tableName`='Club_SessionDates' and `pkValue`='" . makeSafe($selected_id) . "'", $eo);

}

function Club_SessionDates_form($selected_id = '', $AllowUpdate = 1, $AllowInsert = 1, $AllowDelete = 1, $ShowCancel = 0, $TemplateDV = '', $TemplateDVP = '') {
	// function to return an editable form for a table records
	// and fill it with data of record whose ID is $selected_id. If $selected_id
	// is empty, an empty form is shown, with only an 'Add New'
	// button displayed.

	global $Translation;

	// mm: get table permissions
	$arrPerm=getTablePermissions('Club_SessionDates');
	if(!$arrPerm[1] && $selected_id=='') { return ''; }
	$AllowInsert = ($arrPerm[1] ? true : false);
	// print preview?
	$dvprint = false;
	if($selected_id && $_REQUEST['dvprint_x'] != '') {
		$dvprint = true;
	}


	// populate filterers, starting from children to grand-parents

	// unique random identifier
	$rnd1 = ($dvprint ? rand(1000000, 9999999) : '');
	// combobox: Date
	$combo_Date = new DateCombo;
	$combo_Date->DateFormat = "dmy";
	$combo_Date->MinYear = 1900;
	$combo_Date->MaxYear = 2100;
	$combo_Date->DefaultDate = parseMySQLDate('1', '1');
	$combo_Date->MonthNames = $Translation['month names'];
	$combo_Date->NamePrefix = 'Date';
	// combobox: Type
	$combo_Type = new Combo;
	$combo_Type->ListType = 0;
	$combo_Type->MultipleSeparator = ', ';
	$combo_Type->ListBoxHeight = 10;
	$combo_Type->RadiosPerLine = 1;
	if(is_file(dirname(__FILE__).'/hooks/Club_SessionDates.Type.csv')) {
		$Type_data = addslashes(implode('', @file(dirname(__FILE__).'/hooks/Club_SessionDates.Type.csv')));
		$combo_Type->ListItem = explode('||', entitiesToUTF8(convertLegacyOptions($Type_data)));
		$combo_Type->ListData = $combo_Type->ListItem;
	}else{
		$combo_Type->ListItem = explode('||', entitiesToUTF8(convertLegacyOptions("Club Session;;Club Special Events;;Club Track;;Club MTB;;Club CX;;Club TT;;Club Road and Circuit;;Club Race;;")));
		$combo_Type->ListData = $combo_Type->ListItem;
	}
	$combo_Type->SelectName = 'Type';
	// combobox: SubType
	$combo_SubType = new Combo;
	$combo_SubType->ListType = 3;
	$combo_SubType->MultipleSeparator = ', ';
	$combo_SubType->ListBoxHeight = 10;
	$combo_SubType->RadiosPerLine = 1;
	if(is_file(dirname(__FILE__).'/hooks/Club_SessionDates.SubType.csv')) {
		$SubType_data = addslashes(implode('', @file(dirname(__FILE__).'/hooks/Club_SessionDates.SubType.csv')));
		$combo_SubType->ListItem = explode('||', entitiesToUTF8(convertLegacyOptions($SubType_data)));
		$combo_SubType->ListData = $combo_SubType->ListItem;
	}else{
		$combo_SubType->ListItem = explode('||', entitiesToUTF8(convertLegacyOptions("Performance;;Skills;;Race;;Lecture;;Workshop;;Other")));
		$combo_SubType->ListData = $combo_SubType->ListItem;
	}
	$combo_SubType->SelectName = 'SubType';

	if($selected_id) {
		// mm: check member permissions
		if(!$arrPerm[2]) {
			return "";
		}
		// mm: who is the owner?
		$ownerGroupID=sqlValue("select groupID from membership_userrecords where tableName='Club_SessionDates' and pkValue='".makeSafe($selected_id)."'");
		$ownerMemberID=sqlValue("select lcase(memberID) from membership_userrecords where tableName='Club_SessionDates' and pkValue='".makeSafe($selected_id)."'");
		if($arrPerm[2]==1 && getLoggedMemberID()!=$ownerMemberID) {
			return "";
		}
		if($arrPerm[2]==2 && getLoggedGroupID()!=$ownerGroupID) {
			return "";
		}

		// can edit?
		if(($arrPerm[3]==1 && $ownerMemberID==getLoggedMemberID()) || ($arrPerm[3]==2 && $ownerGroupID==getLoggedGroupID()) || $arrPerm[3]==3) {
			$AllowUpdate=1;
		}else{
			$AllowUpdate=0;
		}

		$res = sql("SELECT * FROM `Club_SessionDates` WHERE `Id`='" . makeSafe($selected_id) . "'", $eo);
		if(!($row = db_fetch_array($res))) {
			return error_message($Translation['No records found'], 'Club_SessionDates_view.php', false);
		}
		$combo_Date->DefaultDate = $row['Date'];
		$combo_Type->SelectedData = $row['Type'];
		$combo_SubType->SelectedData = $row['SubType'];
		$urow = $row; /* unsanitized data */
		$hc = new CI_Input();
		$row = $hc->xss_clean($row); /* sanitize data */
	} else {
		$combo_Type->SelectedText = ( $_REQUEST['FilterField'][1]=='3' && $_REQUEST['FilterOperator'][1]=='<=>' ? (get_magic_quotes_gpc() ? stripslashes($_REQUEST['FilterValue'][1]) : $_REQUEST['FilterValue'][1]) : "");
	}
	$combo_Type->Render();
	$combo_SubType->Render();

	// code for template based detail view forms

	// open the detail view template
	if($dvprint) {
		$template_file = is_file("./{$TemplateDVP}") ? "./{$TemplateDVP}" : './templates/Club_SessionDates_templateDVP.html';
		$templateCode = @file_get_contents($template_file);
	}else{
		$template_file = is_file("./{$TemplateDV}") ? "./{$TemplateDV}" : './templates/Club_SessionDates_templateDV.html';
		$templateCode = @file_get_contents($template_file);
	}

	// process form title
	$templateCode = str_replace('<%%DETAIL_VIEW_TITLE%%>', 'Session Details', $templateCode);
	$templateCode = str_replace('<%%RND1%%>', $rnd1, $templateCode);
	$templateCode = str_replace('<%%EMBEDDED%%>', ($_REQUEST['Embedded'] ? 'Embedded=1' : ''), $templateCode);
	// process buttons
	if($AllowInsert) {
		if(!$selected_id) $templateCode = str_replace('<%%INSERT_BUTTON%%>', '<button type="submit" class="btn btn-success" id="insert" name="insert_x" value="1" onclick="return Club_SessionDates_validateData();"><i class="glyphicon glyphicon-plus-sign"></i> ' . $Translation['Save New'] . '</button>', $templateCode);
		$templateCode = str_replace('<%%INSERT_BUTTON%%>', '<button type="submit" class="btn btn-default" id="insert" name="insert_x" value="1" onclick="return Club_SessionDates_validateData();"><i class="glyphicon glyphicon-plus-sign"></i> ' . $Translation['Save As Copy'] . '</button>', $templateCode);
	}else{
		$templateCode = str_replace('<%%INSERT_BUTTON%%>', '', $templateCode);
	}

	// 'Back' button action
	if($_REQUEST['Embedded']) {
		$backAction = 'AppGini.closeParentModal(); return false;';
	}else{
		$backAction = '$j(\'form\').eq(0).attr(\'novalidate\', \'novalidate\'); document.myform.reset(); return true;';
	}

	if($selected_id) {
		if(!$_REQUEST['Embedded']) $templateCode = str_replace('<%%DVPRINT_BUTTON%%>', '<button type="submit" class="btn btn-default" id="dvprint" name="dvprint_x" value="1" onclick="$j(\'form\').eq(0).prop(\'novalidate\', true); document.myform.reset(); return true;" title="' . html_attr($Translation['Print Preview']) . '"><i class="glyphicon glyphicon-print"></i> ' . $Translation['Print Preview'] . '</button>', $templateCode);
		if($AllowUpdate) {
			$templateCode = str_replace('<%%UPDATE_BUTTON%%>', '<button type="submit" class="btn btn-success btn-lg" id="update" name="update_x" value="1" onclick="return Club_SessionDates_validateData();" title="' . html_attr($Translation['Save Changes']) . '"><i class="glyphicon glyphicon-ok"></i> ' . $Translation['Save Changes'] . '</button>', $templateCode);
		}else{
			$templateCode = str_replace('<%%UPDATE_BUTTON%%>', '', $templateCode);
		}
		if(($arrPerm[4]==1 && $ownerMemberID==getLoggedMemberID()) || ($arrPerm[4]==2 && $ownerGroupID==getLoggedGroupID()) || $arrPerm[4]==3) { // allow delete?
			$templateCode = str_replace('<%%DELETE_BUTTON%%>', '<button type="submit" class="btn btn-danger" id="delete" name="delete_x" value="1" onclick="return confirm(\'' . $Translation['are you sure?'] . '\');" title="' . html_attr($Translation['Delete']) . '"><i class="glyphicon glyphicon-trash"></i> ' . $Translation['Delete'] . '</button>', $templateCode);
		}else{
			$templateCode = str_replace('<%%DELETE_BUTTON%%>', '', $templateCode);
		}
		$templateCode = str_replace('<%%DESELECT_BUTTON%%>', '<button type="submit" class="btn btn-default" id="deselect" name="deselect_x" value="1" onclick="' . $backAction . '" title="' . html_attr($Translation['Back']) . '"><i class="glyphicon glyphicon-chevron-left"></i> ' . $Translation['Back'] . '</button>', $templateCode);
	}else{
		$templateCode = str_replace('<%%UPDATE_BUTTON%%>', '', $templateCode);
		$templateCode = str_replace('<%%DELETE_BUTTON%%>', '', $templateCode);
		$templateCode = str_replace('<%%DESELECT_BUTTON%%>', ($ShowCancel ? '<button type="submit" class="btn btn-default" id="deselect" name="deselect_x" value="1" onclick="' . $backAction . '" title="' . html_attr($Translation['Back']) . '"><i class="glyphicon glyphicon-chevron-left"></i> ' . $Translation['Back'] . '</button>' : ''), $templateCode);
	}

	// set records to read only if user can't insert new records and can't edit current record
	if(($selected_id && !$AllowUpdate && !$AllowInsert) || (!$selected_id && !$AllowInsert)) {
		$jsReadOnly .= "\tjQuery('#Date').prop('readonly', true);\n";
		$jsReadOnly .= "\tjQuery('#DateDay, #DateMonth, #DateYear').prop('disabled', true).css({ color: '#555', backgroundColor: '#fff' });\n";
		$jsReadOnly .= "\tjQuery('#Type').replaceWith('<div class=\"form-control-static\" id=\"Type\">' + (jQuery('#Type').val() || '') + '</div>'); jQuery('#Type-multi-selection-help').hide();\n";
		$jsReadOnly .= "\tjQuery('#SubType').replaceWith('<div class=\"form-control-static\" id=\"SubType\">' + (jQuery('#SubType').val() || '') + '</div>'); jQuery('#SubType-multi-selection-help').hide();\n";
		$jsReadOnly .= "\tjQuery('#s2id_SubType').remove();\n";
		$jsReadOnly .= "\tjQuery('.select2-container').hide();\n";

		$noUploads = true;
	}elseif($AllowInsert) {
		$jsEditable .= "\tjQuery('form').eq(0).data('already_changed', true);"; // temporarily disable form change handler
			$jsEditable .= "\tjQuery('form').eq(0).data('already_changed', false);"; // re-enable form change handler
	}

	// process combos
	$templateCode = str_replace('<%%COMBO(Date)%%>', ($selected_id && !$arrPerm[3] ? '<div class="form-control-static">' . $combo_Date->GetHTML(true) . '</div>' : $combo_Date->GetHTML()), $templateCode);
	$templateCode = str_replace('<%%COMBOTEXT(Date)%%>', $combo_Date->GetHTML(true), $templateCode);
	$templateCode = str_replace('<%%COMBO(Type)%%>', $combo_Type->HTML, $templateCode);
	$templateCode = str_replace('<%%COMBOTEXT(Type)%%>', $combo_Type->SelectedData, $templateCode);
	$templateCode = str_replace('<%%COMBO(SubType)%%>', $combo_SubType->HTML, $templateCode);
	$templateCode = str_replace('<%%COMBOTEXT(SubType)%%>', $combo_SubType->SelectedData, $templateCode);

	/* lookup fields array: 'lookup field name' => array('parent table name', 'lookup field caption') */
	$lookup_fields = array();
	foreach($lookup_fields as $luf => $ptfc) {
		$pt_perm = getTablePermissions($ptfc[0]);

		// process foreign key links
		if($pt_perm['view'] || $pt_perm['edit']) {
			$templateCode = str_replace("<%%PLINK({$luf})%%>", '<button type="button" class="btn btn-default view_parent hspacer-md" id="' . $ptfc[0] . '_view_parent" title="' . html_attr($Translation['View'] . ' ' . $ptfc[1]) . '"><i class="glyphicon glyphicon-eye-open"></i></button>', $templateCode);
		}

		// if user has insert permission to parent table of a lookup field, put an add new button
		if($pt_perm['insert'] && !$_REQUEST['Embedded']) {
			$templateCode = str_replace("<%%ADDNEW({$ptfc[0]})%%>", '<button type="button" class="btn btn-success add_new_parent hspacer-md" id="' . $ptfc[0] . '_add_new" title="' . html_attr($Translation['Add New'] . ' ' . $ptfc[1]) . '"><i class="glyphicon glyphicon-plus-sign"></i></button>', $templateCode);
		}
	}

	// process images
	$templateCode = str_replace('<%%UPLOADFILE(Id)%%>', '', $templateCode);
	$templateCode = str_replace('<%%UPLOADFILE(Date)%%>', '', $templateCode);
	$templateCode = str_replace('<%%UPLOADFILE(Type)%%>', '', $templateCode);
	$templateCode = str_replace('<%%UPLOADFILE(SubType)%%>', '', $templateCode);
	$templateCode = str_replace('<%%UPLOADFILE(Notes)%%>', '', $templateCode);

	// process values
	if($selected_id) {
		if( $dvprint) $templateCode = str_replace('<%%VALUE(Id)%%>', safe_html($urow['Id']), $templateCode);
		if(!$dvprint) $templateCode = str_replace('<%%VALUE(Id)%%>', html_attr($row['Id']), $templateCode);
		$templateCode = str_replace('<%%URLVALUE(Id)%%>', urlencode($urow['Id']), $templateCode);
		$templateCode = str_replace('<%%VALUE(Date)%%>', @date('d/m/Y', @strtotime(html_attr($row['Date']))), $templateCode);
		$templateCode = str_replace('<%%URLVALUE(Date)%%>', urlencode(@date('d/m/Y', @strtotime(html_attr($urow['Date'])))), $templateCode);
		if( $dvprint) $templateCode = str_replace('<%%VALUE(Type)%%>', safe_html($urow['Type']), $templateCode);
		if(!$dvprint) $templateCode = str_replace('<%%VALUE(Type)%%>', html_attr($row['Type']), $templateCode);
		$templateCode = str_replace('<%%URLVALUE(Type)%%>', urlencode($urow['Type']), $templateCode);
		if( $dvprint) $templateCode = str_replace('<%%VALUE(SubType)%%>', safe_html($urow['SubType']), $templateCode);
		if(!$dvprint) $templateCode = str_replace('<%%VALUE(SubType)%%>', html_attr($row['SubType']), $templateCode);
		$templateCode = str_replace('<%%URLVALUE(SubType)%%>', urlencode($urow['SubType']), $templateCode);
		if($AllowUpdate || $AllowInsert) {
			$templateCode = str_replace('<%%HTMLAREA(Notes)%%>', '<textarea name="Notes" id="Notes" rows="5">' . html_attr($row['Notes']) . '</textarea>', $templateCode);
		}else{
			$templateCode = str_replace('<%%HTMLAREA(Notes)%%>', '<div id="Notes" class="form-control-static">' . $row['Notes'] . '</div>', $templateCode);
		}
		$templateCode = str_replace('<%%VALUE(Notes)%%>', nl2br($row['Notes']), $templateCode);
		$templateCode = str_replace('<%%URLVALUE(Notes)%%>', urlencode($urow['Notes']), $templateCode);
	}else{
		$templateCode = str_replace('<%%VALUE(Id)%%>', '', $templateCode);
		$templateCode = str_replace('<%%URLVALUE(Id)%%>', urlencode(''), $templateCode);
		$templateCode = str_replace('<%%VALUE(Date)%%>', '1', $templateCode);
		$templateCode = str_replace('<%%URLVALUE(Date)%%>', urlencode('1'), $templateCode);
		$templateCode = str_replace('<%%VALUE(Type)%%>', '', $templateCode);
		$templateCode = str_replace('<%%URLVALUE(Type)%%>', urlencode(''), $templateCode);
		$templateCode = str_replace('<%%VALUE(SubType)%%>', '', $templateCode);
		$templateCode = str_replace('<%%URLVALUE(SubType)%%>', urlencode(''), $templateCode);
		$templateCode = str_replace('<%%HTMLAREA(Notes)%%>', '<textarea name="Notes" id="Notes" rows="5"></textarea>', $templateCode);
	}

	// process translations
	foreach($Translation as $symbol=>$trans) {
		$templateCode = str_replace("<%%TRANSLATION($symbol)%%>", $trans, $templateCode);
	}

	// clear scrap
	$templateCode = str_replace('<%%', '<!-- ', $templateCode);
	$templateCode = str_replace('%%>', ' -->', $templateCode);

	// hide links to inaccessible tables
	if($_REQUEST['dvprint_x'] == '') {
		$templateCode .= "\n\n<script>\$j(function() {\n";
		$arrTables = getTableList();
		foreach($arrTables as $name => $caption) {
			$templateCode .= "\t\$j('#{$name}_link').removeClass('hidden');\n";
			$templateCode .= "\t\$j('#xs_{$name}_link').removeClass('hidden');\n";
		}

		$templateCode .= $jsReadOnly;
		$templateCode .= $jsEditable;

		if(!$selected_id) {
		}

		$templateCode.="\n});</script>\n";
	}

	// ajaxed auto-fill fields
	$templateCode .= '<script>';
	$templateCode .= '$j(function() {';


	$templateCode.="});";
	$templateCode.="</script>";
	$templateCode .= $lookups;

	// handle enforced parent values for read-only lookup fields

	// don't include blank images in lightbox gallery
	$templateCode = preg_replace('/blank.gif" data-lightbox=".*?"/', 'blank.gif"', $templateCode);

	// don't display empty email links
	$templateCode=preg_replace('/<a .*?href="mailto:".*?<\/a>/', '', $templateCode);

	/* default field values */
	$rdata = $jdata = get_defaults('Club_SessionDates');
	if($selected_id) {
		$jdata = get_joined_record('Club_SessionDates', $selected_id);
		if($jdata === false) $jdata = get_defaults('Club_SessionDates');
		$rdata = $row;
	}
	$templateCode .= loadView('Club_SessionDates-ajax-cache', array('rdata' => $rdata, 'jdata' => $jdata));

	// hook: Club_SessionDates_dv
	if(function_exists('Club_SessionDates_dv')) {
		$args=array();
		Club_SessionDates_dv(($selected_id ? $selected_id : FALSE), getMemberInfo(), $templateCode, $args);
	}

	return $templateCode;
}
?>