<?php
	$rdata = array_map('to_utf8', array_map('nl2br', array_map('html_attr_tags_ok', $rdata)));
	$jdata = array_map('to_utf8', array_map('nl2br', array_map('html_attr_tags_ok', $jdata)));
?>
<script>
	$j(function() {
		var tn = 'Club_EventEntrants';

		/* data for selected record, or defaults if none is selected */
		var data = {
			Event: <?php echo json_encode(array('id' => $rdata['Event'], 'value' => $rdata['Event'], 'text' => $jdata['Event'])); ?>,
			Entrant: <?php echo json_encode(array('id' => $rdata['Entrant'], 'value' => $rdata['Entrant'], 'text' => $jdata['Entrant'])); ?>,
			MemberID: <?php echo json_encode($jdata['MemberID']); ?>,
			Licence: <?php echo json_encode($jdata['Licence']); ?>,
			Emergency1: <?php echo json_encode($jdata['Emergency1']); ?>,
			Emergency2: <?php echo json_encode($jdata['Emergency2']); ?>,
			DateOfBirth: <?php echo json_encode($jdata['DateOfBirth']); ?>,
			Age: <?php echo json_encode($jdata['Age']); ?>,
			Gender: <?php echo json_encode($jdata['Gender']); ?>
		};

		/* initialize or continue using AppGini.cache for the current table */
		AppGini.cache = AppGini.cache || {};
		AppGini.cache[tn] = AppGini.cache[tn] || AppGini.ajaxCache();
		var cache = AppGini.cache[tn];

		/* saved value for Event */
		cache.addCheck(function(u, d) {
			if(u != 'ajax_combo.php') return false;
			if(d.t == tn && d.f == 'Event' && d.id == data.Event.id)
				return { results: [ data.Event ], more: false, elapsed: 0.01 };
			return false;
		});

		/* saved value for Entrant */
		cache.addCheck(function(u, d) {
			if(u != 'ajax_combo.php') return false;
			if(d.t == tn && d.f == 'Entrant' && d.id == data.Entrant.id)
				return { results: [ data.Entrant ], more: false, elapsed: 0.01 };
			return false;
		});

		/* saved value for Entrant autofills */
		cache.addCheck(function(u, d) {
			if(u != tn + '_autofill.php') return false;

			for(var rnd in d) if(rnd.match(/^rnd/)) break;

			if(d.mfk == 'Entrant' && d.id == data.Entrant.id) {
				$j('#MemberID' + d[rnd]).html(data.MemberID);
				$j('#Licence' + d[rnd]).html(data.Licence);
				$j('#Emergency1' + d[rnd]).html(data.Emergency1);
				$j('#Emergency2' + d[rnd]).html(data.Emergency2);
				$j('#DateOfBirth' + d[rnd]).html(data.DateOfBirth);
				$j('#Age' + d[rnd]).html(data.Age);
				$j('#Gender' + d[rnd]).html(data.Gender);
				return true;
			}

			return false;
		});

		cache.start();
	});
</script>

