<?php
	$rdata = array_map('to_utf8', array_map('nl2br', array_map('html_attr_tags_ok', $rdata)));
	$jdata = array_map('to_utf8', array_map('nl2br', array_map('html_attr_tags_ok', $jdata)));
?>
<script>
	$j(function() {
		var tn = 'Club_VolunteerAttendance';

		/* data for selected record, or defaults if none is selected */
		var data = {
			Date: <?php echo json_encode(array('id' => $rdata['Date'], 'value' => $rdata['Date'], 'text' => $jdata['Date'])); ?>,
			VolunteerId: <?php echo json_encode(array('id' => $rdata['VolunteerId'], 'value' => $rdata['VolunteerId'], 'text' => $jdata['VolunteerId'])); ?>
		};

		/* initialize or continue using AppGini.cache for the current table */
		AppGini.cache = AppGini.cache || {};
		AppGini.cache[tn] = AppGini.cache[tn] || AppGini.ajaxCache();
		var cache = AppGini.cache[tn];

		/* saved value for Date */
		cache.addCheck(function(u, d) {
			if(u != 'ajax_combo.php') return false;
			if(d.t == tn && d.f == 'Date' && d.id == data.Date.id)
				return { results: [ data.Date ], more: false, elapsed: 0.01 };
			return false;
		});

		/* saved value for VolunteerId */
		cache.addCheck(function(u, d) {
			if(u != 'ajax_combo.php') return false;
			if(d.t == tn && d.f == 'VolunteerId' && d.id == data.VolunteerId.id)
				return { results: [ data.VolunteerId ], more: false, elapsed: 0.01 };
			return false;
		});

		cache.start();
	});
</script>

