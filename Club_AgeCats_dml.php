<?php

// Data functions (insert, update, delete, form) for table Club_AgeCats

// This script and data application were generated by AppGini 5.82
// Download AppGini for free from https://bigprof.com/appgini/download/

function Club_AgeCats_insert() {
	global $Translation;

	// mm: can member insert record?
	$arrPerm = getTablePermissions('Club_AgeCats');
	if(!$arrPerm[1]) return false;

	$data = array();
	$data['Cat'] = $_REQUEST['Cat'];
		if($data['Cat'] == empty_lookup_value) { $data['Cat'] = ''; }
	$data['Description'] = $_REQUEST['Description'];
		if($data['Description'] == empty_lookup_value) { $data['Description'] = ''; }
	$data['StartYearOfBirth'] = $_REQUEST['StartYearOfBirth'];
		if($data['StartYearOfBirth'] == empty_lookup_value) { $data['StartYearOfBirth'] = ''; }
	$data['EndYearOfBirth'] = $_REQUEST['EndYearOfBirth'];
		if($data['EndYearOfBirth'] == empty_lookup_value) { $data['EndYearOfBirth'] = ''; }
	$data['GearRoad'] = $_REQUEST['GearRoad'];
		if($data['GearRoad'] == empty_lookup_value) { $data['GearRoad'] = ''; }
	$data['GearTrack'] = $_REQUEST['GearTrack'];
		if($data['GearTrack'] == empty_lookup_value) { $data['GearTrack'] = ''; }
	$data['GearRoller'] = $_REQUEST['GearRoller'];
		if($data['GearRoller'] == empty_lookup_value) { $data['GearRoller'] = ''; }

	// hook: Club_AgeCats_before_insert
	if(function_exists('Club_AgeCats_before_insert')) {
		$args = array();
		if(!Club_AgeCats_before_insert($data, getMemberInfo(), $args)) { return false; }
	}

	$error = '';
	// set empty fields to NULL
	$data = array_map(function($v) { return ($v === '' ? NULL : $v); }, $data);
	insert('Club_AgeCats', backtick_keys_once($data), $error);
	if($error)
		die("{$error}<br><a href=\"#\" onclick=\"history.go(-1);\">{$Translation['< back']}</a>");

	$recID = db_insert_id(db_link());

	// hook: Club_AgeCats_after_insert
	if(function_exists('Club_AgeCats_after_insert')) {
		$res = sql("select * from `Club_AgeCats` where `Id`='" . makeSafe($recID, false) . "' limit 1", $eo);
		if($row = db_fetch_assoc($res)) {
			$data = array_map('makeSafe', $row);
		}
		$data['selectedID'] = makeSafe($recID, false);
		$args=array();
		if(!Club_AgeCats_after_insert($data, getMemberInfo(), $args)) { return $recID; }
	}

	// mm: save ownership data
	set_record_owner('Club_AgeCats', $recID, getLoggedMemberID());

	// if this record is a copy of another record, copy children if applicable
	if(!empty($_REQUEST['SelectedID'])) Club_AgeCats_copy_children($recID, $_REQUEST['SelectedID']);

	return $recID;
}

function Club_AgeCats_copy_children($destination_id, $source_id) {
	global $Translation;
	$requests = array(); // array of curl handlers for launching insert requests
	$eo = array('silentErrors' => true);
	$uploads_dir = realpath(dirname(__FILE__) . '/../' . $Translation['ImageFolder']);
	$safe_sid = makeSafe($source_id);

	// launch requests, asynchronously
	curl_batch($requests);
}

function Club_AgeCats_delete($selected_id, $AllowDeleteOfParents=false, $skipChecks=false) {
	// insure referential integrity ...
	global $Translation;
	$selected_id=makeSafe($selected_id);

	// mm: can member delete record?
	$arrPerm=getTablePermissions('Club_AgeCats');
	$ownerGroupID=sqlValue("select groupID from membership_userrecords where tableName='Club_AgeCats' and pkValue='$selected_id'");
	$ownerMemberID=sqlValue("select lcase(memberID) from membership_userrecords where tableName='Club_AgeCats' and pkValue='$selected_id'");
	if(($arrPerm[4]==1 && $ownerMemberID==getLoggedMemberID()) || ($arrPerm[4]==2 && $ownerGroupID==getLoggedGroupID()) || $arrPerm[4]==3) { // allow delete?
		// delete allowed, so continue ...
	}else{
		return $Translation['You don\'t have enough permissions to delete this record'];
	}

	// hook: Club_AgeCats_before_delete
	if(function_exists('Club_AgeCats_before_delete')) {
		$args=array();
		if(!Club_AgeCats_before_delete($selected_id, $skipChecks, getMemberInfo(), $args))
			return $Translation['Couldn\'t delete this record'];
	}

	sql("delete from `Club_AgeCats` where `Id`='$selected_id'", $eo);

	// hook: Club_AgeCats_after_delete
	if(function_exists('Club_AgeCats_after_delete')) {
		$args=array();
		Club_AgeCats_after_delete($selected_id, getMemberInfo(), $args);
	}

	// mm: delete ownership data
	sql("delete from membership_userrecords where tableName='Club_AgeCats' and pkValue='$selected_id'", $eo);
}

function Club_AgeCats_update($selected_id) {
	global $Translation;

	// mm: can member edit record?
	$arrPerm=getTablePermissions('Club_AgeCats');
	$ownerGroupID=sqlValue("select groupID from membership_userrecords where tableName='Club_AgeCats' and pkValue='".makeSafe($selected_id)."'");
	$ownerMemberID=sqlValue("select lcase(memberID) from membership_userrecords where tableName='Club_AgeCats' and pkValue='".makeSafe($selected_id)."'");
	if(($arrPerm[3]==1 && $ownerMemberID==getLoggedMemberID()) || ($arrPerm[3]==2 && $ownerGroupID==getLoggedGroupID()) || $arrPerm[3]==3) { // allow update?
		// update allowed, so continue ...
	}else{
		return false;
	}

	$data['Cat'] = makeSafe($_REQUEST['Cat']);
		if($data['Cat'] == empty_lookup_value) { $data['Cat'] = ''; }
	$data['Description'] = makeSafe($_REQUEST['Description']);
		if($data['Description'] == empty_lookup_value) { $data['Description'] = ''; }
	$data['StartYearOfBirth'] = makeSafe($_REQUEST['StartYearOfBirth']);
		if($data['StartYearOfBirth'] == empty_lookup_value) { $data['StartYearOfBirth'] = ''; }
	$data['EndYearOfBirth'] = makeSafe($_REQUEST['EndYearOfBirth']);
		if($data['EndYearOfBirth'] == empty_lookup_value) { $data['EndYearOfBirth'] = ''; }
	$data['GearRoad'] = makeSafe($_REQUEST['GearRoad']);
		if($data['GearRoad'] == empty_lookup_value) { $data['GearRoad'] = ''; }
	$data['GearTrack'] = makeSafe($_REQUEST['GearTrack']);
		if($data['GearTrack'] == empty_lookup_value) { $data['GearTrack'] = ''; }
	$data['GearRoller'] = makeSafe($_REQUEST['GearRoller']);
		if($data['GearRoller'] == empty_lookup_value) { $data['GearRoller'] = ''; }
	$data['selectedID'] = makeSafe($selected_id);

	// hook: Club_AgeCats_before_update
	if(function_exists('Club_AgeCats_before_update')) {
		$args = array();
		if(!Club_AgeCats_before_update($data, getMemberInfo(), $args)) { return false; }
	}

	$o = array('silentErrors' => true);
	sql('update `Club_AgeCats` set       `Cat`=' . (($data['Cat'] !== '' && $data['Cat'] !== NULL) ? "'{$data['Cat']}'" : 'NULL') . ', `Description`=' . (($data['Description'] !== '' && $data['Description'] !== NULL) ? "'{$data['Description']}'" : 'NULL') . ', `StartYearOfBirth`=' . (($data['StartYearOfBirth'] !== '' && $data['StartYearOfBirth'] !== NULL) ? "'{$data['StartYearOfBirth']}'" : 'NULL') . ', `EndYearOfBirth`=' . (($data['EndYearOfBirth'] !== '' && $data['EndYearOfBirth'] !== NULL) ? "'{$data['EndYearOfBirth']}'" : 'NULL') . ', `GearRoad`=' . (($data['GearRoad'] !== '' && $data['GearRoad'] !== NULL) ? "'{$data['GearRoad']}'" : 'NULL') . ', `GearTrack`=' . (($data['GearTrack'] !== '' && $data['GearTrack'] !== NULL) ? "'{$data['GearTrack']}'" : 'NULL') . ', `GearRoller`=' . (($data['GearRoller'] !== '' && $data['GearRoller'] !== NULL) ? "'{$data['GearRoller']}'" : 'NULL') . " where `Id`='".makeSafe($selected_id)."'", $o);
	if($o['error']!='') {
		echo $o['error'];
		echo '<a href="Club_AgeCats_view.php?SelectedID='.urlencode($selected_id)."\">{$Translation['< back']}</a>";
		exit;
	}


	// hook: Club_AgeCats_after_update
	if(function_exists('Club_AgeCats_after_update')) {
		$res = sql("SELECT * FROM `Club_AgeCats` WHERE `Id`='{$data['selectedID']}' LIMIT 1", $eo);
		if($row = db_fetch_assoc($res)) {
			$data = array_map('makeSafe', $row);
		}
		$data['selectedID'] = $data['Id'];
		$args = array();
		if(!Club_AgeCats_after_update($data, getMemberInfo(), $args)) { return; }
	}

	// mm: update ownership data
	sql("update `membership_userrecords` set `dateUpdated`='" . time() . "' where `tableName`='Club_AgeCats' and `pkValue`='" . makeSafe($selected_id) . "'", $eo);

}

function Club_AgeCats_form($selected_id = '', $AllowUpdate = 1, $AllowInsert = 1, $AllowDelete = 1, $ShowCancel = 0, $TemplateDV = '', $TemplateDVP = '') {
	// function to return an editable form for a table records
	// and fill it with data of record whose ID is $selected_id. If $selected_id
	// is empty, an empty form is shown, with only an 'Add New'
	// button displayed.

	global $Translation;

	// mm: get table permissions
	$arrPerm=getTablePermissions('Club_AgeCats');
	if(!$arrPerm[1] && $selected_id=='') { return ''; }
	$AllowInsert = ($arrPerm[1] ? true : false);
	// print preview?
	$dvprint = false;
	if($selected_id && $_REQUEST['dvprint_x'] != '') {
		$dvprint = true;
	}


	// populate filterers, starting from children to grand-parents

	// unique random identifier
	$rnd1 = ($dvprint ? rand(1000000, 9999999) : '');

	if($selected_id) {
		// mm: check member permissions
		if(!$arrPerm[2]) {
			return "";
		}
		// mm: who is the owner?
		$ownerGroupID=sqlValue("select groupID from membership_userrecords where tableName='Club_AgeCats' and pkValue='".makeSafe($selected_id)."'");
		$ownerMemberID=sqlValue("select lcase(memberID) from membership_userrecords where tableName='Club_AgeCats' and pkValue='".makeSafe($selected_id)."'");
		if($arrPerm[2]==1 && getLoggedMemberID()!=$ownerMemberID) {
			return "";
		}
		if($arrPerm[2]==2 && getLoggedGroupID()!=$ownerGroupID) {
			return "";
		}

		// can edit?
		if(($arrPerm[3]==1 && $ownerMemberID==getLoggedMemberID()) || ($arrPerm[3]==2 && $ownerGroupID==getLoggedGroupID()) || $arrPerm[3]==3) {
			$AllowUpdate=1;
		}else{
			$AllowUpdate=0;
		}

		$res = sql("SELECT * FROM `Club_AgeCats` WHERE `Id`='" . makeSafe($selected_id) . "'", $eo);
		if(!($row = db_fetch_array($res))) {
			return error_message($Translation['No records found'], 'Club_AgeCats_view.php', false);
		}
		$urow = $row; /* unsanitized data */
		$hc = new CI_Input();
		$row = $hc->xss_clean($row); /* sanitize data */
	} else {
	}

	ob_start();
	?>

	<script>
		// initial lookup values

		jQuery(function() {
			setTimeout(function() {
			}, 10); /* we need to slightly delay client-side execution of the above code to allow AppGini.ajaxCache to work */
		});
	</script>
	<?php

	$lookups = str_replace('__RAND__', $rnd1, ob_get_contents());
	ob_end_clean();


	// code for template based detail view forms

	// open the detail view template
	if($dvprint) {
		$template_file = is_file("./{$TemplateDVP}") ? "./{$TemplateDVP}" : './templates/Club_AgeCats_templateDVP.html';
		$templateCode = @file_get_contents($template_file);
	}else{
		$template_file = is_file("./{$TemplateDV}") ? "./{$TemplateDV}" : './templates/Club_AgeCats_templateDV.html';
		$templateCode = @file_get_contents($template_file);
	}

	// process form title
	$templateCode = str_replace('<%%DETAIL_VIEW_TITLE%%>', 'Age Cat Details', $templateCode);
	$templateCode = str_replace('<%%RND1%%>', $rnd1, $templateCode);
	$templateCode = str_replace('<%%EMBEDDED%%>', ($_REQUEST['Embedded'] ? 'Embedded=1' : ''), $templateCode);
	// process buttons
	if($AllowInsert) {
		if(!$selected_id) $templateCode = str_replace('<%%INSERT_BUTTON%%>', '<button type="submit" class="btn btn-success" id="insert" name="insert_x" value="1" onclick="return Club_AgeCats_validateData();"><i class="glyphicon glyphicon-plus-sign"></i> ' . $Translation['Save New'] . '</button>', $templateCode);
		$templateCode = str_replace('<%%INSERT_BUTTON%%>', '<button type="submit" class="btn btn-default" id="insert" name="insert_x" value="1" onclick="return Club_AgeCats_validateData();"><i class="glyphicon glyphicon-plus-sign"></i> ' . $Translation['Save As Copy'] . '</button>', $templateCode);
	}else{
		$templateCode = str_replace('<%%INSERT_BUTTON%%>', '', $templateCode);
	}

	// 'Back' button action
	if($_REQUEST['Embedded']) {
		$backAction = 'AppGini.closeParentModal(); return false;';
	}else{
		$backAction = '$j(\'form\').eq(0).attr(\'novalidate\', \'novalidate\'); document.myform.reset(); return true;';
	}

	if($selected_id) {
		if(!$_REQUEST['Embedded']) $templateCode = str_replace('<%%DVPRINT_BUTTON%%>', '<button type="submit" class="btn btn-default" id="dvprint" name="dvprint_x" value="1" onclick="$j(\'form\').eq(0).prop(\'novalidate\', true); document.myform.reset(); return true;" title="' . html_attr($Translation['Print Preview']) . '"><i class="glyphicon glyphicon-print"></i> ' . $Translation['Print Preview'] . '</button>', $templateCode);
		if($AllowUpdate) {
			$templateCode = str_replace('<%%UPDATE_BUTTON%%>', '<button type="submit" class="btn btn-success btn-lg" id="update" name="update_x" value="1" onclick="return Club_AgeCats_validateData();" title="' . html_attr($Translation['Save Changes']) . '"><i class="glyphicon glyphicon-ok"></i> ' . $Translation['Save Changes'] . '</button>', $templateCode);
		}else{
			$templateCode = str_replace('<%%UPDATE_BUTTON%%>', '', $templateCode);
		}
		if(($arrPerm[4]==1 && $ownerMemberID==getLoggedMemberID()) || ($arrPerm[4]==2 && $ownerGroupID==getLoggedGroupID()) || $arrPerm[4]==3) { // allow delete?
			$templateCode = str_replace('<%%DELETE_BUTTON%%>', '<button type="submit" class="btn btn-danger" id="delete" name="delete_x" value="1" onclick="return confirm(\'' . $Translation['are you sure?'] . '\');" title="' . html_attr($Translation['Delete']) . '"><i class="glyphicon glyphicon-trash"></i> ' . $Translation['Delete'] . '</button>', $templateCode);
		}else{
			$templateCode = str_replace('<%%DELETE_BUTTON%%>', '', $templateCode);
		}
		$templateCode = str_replace('<%%DESELECT_BUTTON%%>', '<button type="submit" class="btn btn-default" id="deselect" name="deselect_x" value="1" onclick="' . $backAction . '" title="' . html_attr($Translation['Back']) . '"><i class="glyphicon glyphicon-chevron-left"></i> ' . $Translation['Back'] . '</button>', $templateCode);
	}else{
		$templateCode = str_replace('<%%UPDATE_BUTTON%%>', '', $templateCode);
		$templateCode = str_replace('<%%DELETE_BUTTON%%>', '', $templateCode);
		$templateCode = str_replace('<%%DESELECT_BUTTON%%>', ($ShowCancel ? '<button type="submit" class="btn btn-default" id="deselect" name="deselect_x" value="1" onclick="' . $backAction . '" title="' . html_attr($Translation['Back']) . '"><i class="glyphicon glyphicon-chevron-left"></i> ' . $Translation['Back'] . '</button>' : ''), $templateCode);
	}

	// set records to read only if user can't insert new records and can't edit current record
	if(($selected_id && !$AllowUpdate && !$AllowInsert) || (!$selected_id && !$AllowInsert)) {
		$jsReadOnly .= "\tjQuery('#Cat').replaceWith('<div class=\"form-control-static\" id=\"Cat\">' + (jQuery('#Cat').val() || '') + '</div>');\n";
		$jsReadOnly .= "\tjQuery('#Description').replaceWith('<div class=\"form-control-static\" id=\"Description\">' + (jQuery('#Description').val() || '') + '</div>');\n";
		$jsReadOnly .= "\tjQuery('#StartYearOfBirth').replaceWith('<div class=\"form-control-static\" id=\"StartYearOfBirth\">' + (jQuery('#StartYearOfBirth').val() || '') + '</div>');\n";
		$jsReadOnly .= "\tjQuery('#EndYearOfBirth').replaceWith('<div class=\"form-control-static\" id=\"EndYearOfBirth\">' + (jQuery('#EndYearOfBirth').val() || '') + '</div>');\n";
		$jsReadOnly .= "\tjQuery('#GearRoad').replaceWith('<div class=\"form-control-static\" id=\"GearRoad\">' + (jQuery('#GearRoad').val() || '') + '</div>');\n";
		$jsReadOnly .= "\tjQuery('#GearTrack').replaceWith('<div class=\"form-control-static\" id=\"GearTrack\">' + (jQuery('#GearTrack').val() || '') + '</div>');\n";
		$jsReadOnly .= "\tjQuery('#GearRoller').replaceWith('<div class=\"form-control-static\" id=\"GearRoller\">' + (jQuery('#GearRoller').val() || '') + '</div>');\n";
		$jsReadOnly .= "\tjQuery('.select2-container').hide();\n";

		$noUploads = true;
	}elseif($AllowInsert) {
		$jsEditable .= "\tjQuery('form').eq(0).data('already_changed', true);"; // temporarily disable form change handler
			$jsEditable .= "\tjQuery('form').eq(0).data('already_changed', false);"; // re-enable form change handler
	}

	// process combos

	/* lookup fields array: 'lookup field name' => array('parent table name', 'lookup field caption') */
	$lookup_fields = array();
	foreach($lookup_fields as $luf => $ptfc) {
		$pt_perm = getTablePermissions($ptfc[0]);

		// process foreign key links
		if($pt_perm['view'] || $pt_perm['edit']) {
			$templateCode = str_replace("<%%PLINK({$luf})%%>", '<button type="button" class="btn btn-default view_parent hspacer-md" id="' . $ptfc[0] . '_view_parent" title="' . html_attr($Translation['View'] . ' ' . $ptfc[1]) . '"><i class="glyphicon glyphicon-eye-open"></i></button>', $templateCode);
		}

		// if user has insert permission to parent table of a lookup field, put an add new button
		if($pt_perm['insert'] && !$_REQUEST['Embedded']) {
			$templateCode = str_replace("<%%ADDNEW({$ptfc[0]})%%>", '<button type="button" class="btn btn-success add_new_parent hspacer-md" id="' . $ptfc[0] . '_add_new" title="' . html_attr($Translation['Add New'] . ' ' . $ptfc[1]) . '"><i class="glyphicon glyphicon-plus-sign"></i></button>', $templateCode);
		}
	}

	// process images
	$templateCode = str_replace('<%%UPLOADFILE(Id)%%>', '', $templateCode);
	$templateCode = str_replace('<%%UPLOADFILE(Cat)%%>', '', $templateCode);
	$templateCode = str_replace('<%%UPLOADFILE(Description)%%>', '', $templateCode);
	$templateCode = str_replace('<%%UPLOADFILE(StartYearOfBirth)%%>', '', $templateCode);
	$templateCode = str_replace('<%%UPLOADFILE(EndYearOfBirth)%%>', '', $templateCode);
	$templateCode = str_replace('<%%UPLOADFILE(GearRoad)%%>', '', $templateCode);
	$templateCode = str_replace('<%%UPLOADFILE(GearTrack)%%>', '', $templateCode);
	$templateCode = str_replace('<%%UPLOADFILE(GearRoller)%%>', '', $templateCode);

	// process values
	if($selected_id) {
		if( $dvprint) $templateCode = str_replace('<%%VALUE(Id)%%>', safe_html($urow['Id']), $templateCode);
		if(!$dvprint) $templateCode = str_replace('<%%VALUE(Id)%%>', html_attr($row['Id']), $templateCode);
		$templateCode = str_replace('<%%URLVALUE(Id)%%>', urlencode($urow['Id']), $templateCode);
		if( $dvprint) $templateCode = str_replace('<%%VALUE(Cat)%%>', safe_html($urow['Cat']), $templateCode);
		if(!$dvprint) $templateCode = str_replace('<%%VALUE(Cat)%%>', html_attr($row['Cat']), $templateCode);
		$templateCode = str_replace('<%%URLVALUE(Cat)%%>', urlencode($urow['Cat']), $templateCode);
		if( $dvprint) $templateCode = str_replace('<%%VALUE(Description)%%>', safe_html($urow['Description']), $templateCode);
		if(!$dvprint) $templateCode = str_replace('<%%VALUE(Description)%%>', html_attr($row['Description']), $templateCode);
		$templateCode = str_replace('<%%URLVALUE(Description)%%>', urlencode($urow['Description']), $templateCode);
		if( $dvprint) $templateCode = str_replace('<%%VALUE(StartYearOfBirth)%%>', safe_html($urow['StartYearOfBirth']), $templateCode);
		if(!$dvprint) $templateCode = str_replace('<%%VALUE(StartYearOfBirth)%%>', html_attr($row['StartYearOfBirth']), $templateCode);
		$templateCode = str_replace('<%%URLVALUE(StartYearOfBirth)%%>', urlencode($urow['StartYearOfBirth']), $templateCode);
		if( $dvprint) $templateCode = str_replace('<%%VALUE(EndYearOfBirth)%%>', safe_html($urow['EndYearOfBirth']), $templateCode);
		if(!$dvprint) $templateCode = str_replace('<%%VALUE(EndYearOfBirth)%%>', html_attr($row['EndYearOfBirth']), $templateCode);
		$templateCode = str_replace('<%%URLVALUE(EndYearOfBirth)%%>', urlencode($urow['EndYearOfBirth']), $templateCode);
		if( $dvprint) $templateCode = str_replace('<%%VALUE(GearRoad)%%>', safe_html($urow['GearRoad']), $templateCode);
		if(!$dvprint) $templateCode = str_replace('<%%VALUE(GearRoad)%%>', html_attr($row['GearRoad']), $templateCode);
		$templateCode = str_replace('<%%URLVALUE(GearRoad)%%>', urlencode($urow['GearRoad']), $templateCode);
		if( $dvprint) $templateCode = str_replace('<%%VALUE(GearTrack)%%>', safe_html($urow['GearTrack']), $templateCode);
		if(!$dvprint) $templateCode = str_replace('<%%VALUE(GearTrack)%%>', html_attr($row['GearTrack']), $templateCode);
		$templateCode = str_replace('<%%URLVALUE(GearTrack)%%>', urlencode($urow['GearTrack']), $templateCode);
		if( $dvprint) $templateCode = str_replace('<%%VALUE(GearRoller)%%>', safe_html($urow['GearRoller']), $templateCode);
		if(!$dvprint) $templateCode = str_replace('<%%VALUE(GearRoller)%%>', html_attr($row['GearRoller']), $templateCode);
		$templateCode = str_replace('<%%URLVALUE(GearRoller)%%>', urlencode($urow['GearRoller']), $templateCode);
	}else{
		$templateCode = str_replace('<%%VALUE(Id)%%>', '', $templateCode);
		$templateCode = str_replace('<%%URLVALUE(Id)%%>', urlencode(''), $templateCode);
		$templateCode = str_replace('<%%VALUE(Cat)%%>', '', $templateCode);
		$templateCode = str_replace('<%%URLVALUE(Cat)%%>', urlencode(''), $templateCode);
		$templateCode = str_replace('<%%VALUE(Description)%%>', '', $templateCode);
		$templateCode = str_replace('<%%URLVALUE(Description)%%>', urlencode(''), $templateCode);
		$templateCode = str_replace('<%%VALUE(StartYearOfBirth)%%>', '', $templateCode);
		$templateCode = str_replace('<%%URLVALUE(StartYearOfBirth)%%>', urlencode(''), $templateCode);
		$templateCode = str_replace('<%%VALUE(EndYearOfBirth)%%>', '', $templateCode);
		$templateCode = str_replace('<%%URLVALUE(EndYearOfBirth)%%>', urlencode(''), $templateCode);
		$templateCode = str_replace('<%%VALUE(GearRoad)%%>', '', $templateCode);
		$templateCode = str_replace('<%%URLVALUE(GearRoad)%%>', urlencode(''), $templateCode);
		$templateCode = str_replace('<%%VALUE(GearTrack)%%>', '', $templateCode);
		$templateCode = str_replace('<%%URLVALUE(GearTrack)%%>', urlencode(''), $templateCode);
		$templateCode = str_replace('<%%VALUE(GearRoller)%%>', '', $templateCode);
		$templateCode = str_replace('<%%URLVALUE(GearRoller)%%>', urlencode(''), $templateCode);
	}

	// process translations
	foreach($Translation as $symbol=>$trans) {
		$templateCode = str_replace("<%%TRANSLATION($symbol)%%>", $trans, $templateCode);
	}

	// clear scrap
	$templateCode = str_replace('<%%', '<!-- ', $templateCode);
	$templateCode = str_replace('%%>', ' -->', $templateCode);

	// hide links to inaccessible tables
	if($_REQUEST['dvprint_x'] == '') {
		$templateCode .= "\n\n<script>\$j(function() {\n";
		$arrTables = getTableList();
		foreach($arrTables as $name => $caption) {
			$templateCode .= "\t\$j('#{$name}_link').removeClass('hidden');\n";
			$templateCode .= "\t\$j('#xs_{$name}_link').removeClass('hidden');\n";
		}

		$templateCode .= $jsReadOnly;
		$templateCode .= $jsEditable;

		if(!$selected_id) {
		}

		$templateCode.="\n});</script>\n";
	}

	// ajaxed auto-fill fields
	$templateCode .= '<script>';
	$templateCode .= '$j(function() {';


	$templateCode.="});";
	$templateCode.="</script>";
	$templateCode .= $lookups;

	// handle enforced parent values for read-only lookup fields

	// don't include blank images in lightbox gallery
	$templateCode = preg_replace('/blank.gif" data-lightbox=".*?"/', 'blank.gif"', $templateCode);

	// don't display empty email links
	$templateCode=preg_replace('/<a .*?href="mailto:".*?<\/a>/', '', $templateCode);

	/* default field values */
	$rdata = $jdata = get_defaults('Club_AgeCats');
	if($selected_id) {
		$jdata = get_joined_record('Club_AgeCats', $selected_id);
		if($jdata === false) $jdata = get_defaults('Club_AgeCats');
		$rdata = $row;
	}
	$templateCode .= loadView('Club_AgeCats-ajax-cache', array('rdata' => $rdata, 'jdata' => $jdata));

	// hook: Club_AgeCats_dv
	if(function_exists('Club_AgeCats_dv')) {
		$args=array();
		Club_AgeCats_dv(($selected_id ? $selected_id : FALSE), getMemberInfo(), $templateCode, $args);
	}

	return $templateCode;
}
?>